package com.mcprohosting.plugins.ssundee_hub.listeners;

import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic.DynamicLeaderboardSigns;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerSigns;
import com.mcprohosting.plugins.ssundee_hub.utilities.LocationUtil;
import com.mcprohosting.plugins.ssundee_hub.utilities.UtilityMethods;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignListeners implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Player player = event.getPlayer();

        if (!block.getType().equals(Material.WALL_SIGN)) {
            return;
        }

        if (ServerSigns.isRegisteredSign(block)) {
            if (player.hasPermission("ssundeehub.editserversigns")) {
                ServerSigns.removeSign(block);
                player.sendMessage("Server teleport sign removed.");
            } else {
                player.sendMessage(ChatColor.RED + "You do not have permission to remove server teleport signs.");
                event.setCancelled(true);
            }
        }

        if (DynamicLeaderboardSigns.isRegisteredSign(block)) {
            if (player.hasPermission("ssundeehub.editlbsigns")) {
                DynamicLeaderboardSigns.removeSign(block);
                player.sendMessage("Leaderboard sign removed.");
            } else {
                player.sendMessage(ChatColor.RED + "You do not have permission to remove leaderboard signs.");
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        // Handle player who is clicking to set a server sign or leaderboard sign
        if (ServerSigns.isPlayerEditing(player.getName())) {
            if (!block.getType().equals(Material.WALL_SIGN)) {
                player.sendMessage(ChatColor.RED + "Please click on a sign.");
            } else {
                String[] args = ServerSigns.endPlayerEditing(player.getName());
                ServerSigns.addSign(block, ChatColor.translateAlternateColorCodes('&', args[0]), args[1]);
                player.sendMessage("Server sign added for " + args[1]);
            }

            return;
        } else if (DynamicLeaderboardSigns.isPlayerEditing(player.getName())) {
            if (!block.getType().equals(Material.WALL_SIGN)) {
                player.sendMessage(ChatColor.RED + "Please click on a sign.");
            } else {
                DynamicLeaderboardSigns.addSign(block);
                player.sendMessage("Leaderboard sign added.");
            }

            return;
        } else if (LocationUtil.isEditing(player.getName())) {
            if (!block.getType().equals(Material.WALL_SIGN)) {
                player.sendMessage(ChatColor.RED + "Please click on a sign!");
                return;
            }

            LocationUtil.addLocation(player.getName(), block.getLocation());
            player.sendMessage("SG leaderboard sign set!");
        }

        // Handle player who is clicking to teleport
        if (block.getType().equals(Material.WALL_SIGN)) {
            if (ServerSigns.isRegisteredSign(event.getClickedBlock())) {
                User user = User.getUser(player.getName());

                if (ServerSigns.getServerStatus(event.getClickedBlock()).equalsIgnoreCase("restarting")) {
                    player.sendMessage("This server is restarting!");
                    return;
                }

                if (user.getRank().equalsIgnoreCase("DEFAULT") && ServerSigns.isServerFull(event.getClickedBlock())) {
                    player.sendMessage(ChatColor.RED +
                            "This server is currently full. Please select a different server.");
                } else {
                    UtilityMethods.redirectPlayerToServer(player, ServerSigns.getServerName(event.getClickedBlock()));
                }
            }
        }
    }
}
