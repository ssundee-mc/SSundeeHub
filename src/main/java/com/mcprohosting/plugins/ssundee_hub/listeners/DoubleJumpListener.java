package com.mcprohosting.plugins.ssundee_hub.listeners;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

public class DoubleJumpListener implements Listener {
    public DoubleJumpListener() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(SSundeeHub.getPlugin(), new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (player.getGameMode().equals(GameMode.CREATIVE)) {
                        return;
                    }

                    if (!isEnabled(player)) {
                        return;
                    }

                    if (player.isOnGround()) {
                        player.setAllowFlight(true);
                    }
                }
            }
        }, 0, 10);
    }

    @EventHandler
    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
        Player player = event.getPlayer();

        if (event.isFlying() && isEnabled(player) && !player.getGameMode().equals(GameMode.CREATIVE)) {
            player.setAllowFlight(false);
            event.setCancelled(true);

            double pitch = Math.toRadians(player.getLocation().getPitch());
            double yaw = Math.toRadians(player.getLocation().getYaw());
            player.setVelocity(new Vector(Math.cos(pitch) * Math.sin(yaw) * -1, 0.75 + Math.abs(Math.sin(pitch)) * 0.5, Math.cos(pitch) * Math.cos(yaw)));
        }
    }

    private boolean isEnabled(Player player) {
        User user = User.getUser(player.getName());
        return (user.getTravelPowerLevel(TravelPower.DOUBLE_JUMP) == 1) && !user.isTravelPowerActive(TravelPower.FLY);
    }
}
