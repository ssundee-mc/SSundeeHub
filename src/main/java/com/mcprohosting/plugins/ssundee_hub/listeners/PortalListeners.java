package com.mcprohosting.plugins.ssundee_hub.listeners;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerPortals;
import com.mcprohosting.plugins.ssundee_hub.utilities.UtilityMethods;
import com.mcprohosting.plugins.ssundee_hub.utilities.map.ServerPortal;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class PortalListeners implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerMove(PlayerMoveEvent event) {
        for (ServerPortal portal : SSundeeHub.grabConfig().serverportals.values()) {
            if (portal.entered(event.getPlayer())) {
                UtilityMethods.redirectPlayerToServer(event.getPlayer(), ServerPortals.getServer(portal));
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        // Handle player who is clicking to set a server portal
        if (ServerPortals.isPlayerEditing(player.getName())) {
            User user = User.getUser(player.getName());
            user.updatePortal(block);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getPlayer().hasPermission("ssundeehub.editserverportals") == false) {
            event.setCancelled(true);
            return;
        }

        ServerPortals.removePortal(event.getBlock());
    }

}
