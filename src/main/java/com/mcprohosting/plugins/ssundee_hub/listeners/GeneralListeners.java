package com.mcprohosting.plugins.ssundee_hub.listeners;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.commands.ShutUp;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.Pet;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.UtilityMethods;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuTools;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.ProcessMenu;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeneralListeners implements Listener {
    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!UtilityMethods.canChangeBlocks(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!UtilityMethods.canChangeBlocks(event.getPlayer())) {
            event.setCancelled(true);
            event.getPlayer().updateInventory();
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Location spawnLocation = SSundeeHub.grabConfig().settings_spawn.getLocation();

        if (spawnLocation.getBlockY() != 0) {
            event.setRespawnLocation(spawnLocation);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        event.setDeathMessage(null);
    }

    @EventHandler(ignoreCancelled = false)
    public void onPlayerJoin(PlayerJoinEvent event) {
        // Check if database contains a player
        if (!DatabaseManager.containsPlayer(event.getPlayer().getName())) {
            DatabaseManager.addPlayer(event.getPlayer().getName());
        }

        // Check if user instance already exists
        User user;
        if (User.containsUser(event.getPlayer().getName()) == false) {
            user = new User(event.getPlayer().getName());
            user.setMenuState(MenuState.NONE);
            user.addUser(event.getPlayer().getName());
        } else {
            user = User.getUser(event.getPlayer().getName());
        }

        // No player join message
        event.setJoinMessage(null);

        // Set player gamemode to adventure mode
        event.getPlayer().setGameMode(GameMode.ADVENTURE);

        // Setup Inventory
        event.getPlayer().getInventory().clear();
        ProcessMenu.fillPlayerInventoryWithItems(event.getPlayer());

        // Set active pet
        Pet activePet = DatabaseManager.getActivePetForPlayer(event.getPlayer());
        if (activePet != null) {
            User u = User.getUser(event.getPlayer().getName());
            u.currentPet = activePet.spawnPet();
        }

        // Teleport player to spawn
        Location spawnLocation = SSundeeHub.grabConfig().settings_spawn.getLocation();
        if (spawnLocation.getBlockY() != 0) {
            event.getPlayer().teleport(spawnLocation);
        }

        // Welcome the user
        for (String line : SSundeeHub.grabConfig().settings_message) {
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', line));
        }

        // Vanish player for other users
        for (User u : User.getUsers()) {
            if (u.getVanishState() == 1 || u.getVanishState() == 2) {
                Bukkit.getPlayer(u.getName()).hidePlayer(event.getPlayer());
            }
        }

        switch (user.getRank().toString().toLowerCase()) {
            case "default":
            case "lapis":
            case "redstone":
            case "gold":
            case "diamond":
                user.setMuted(ShutUp.getMuted());
            default:
                break;
        }

        event.getPlayer().setAllowFlight(false);

    }

    @EventHandler(ignoreCancelled = false)
    public void onInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(SSundeeHub.getPlugin(), new ProcessMenu(event.getPlayer()));
        } else if (event.getClickedBlock().getType() != Material.CHEST) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(SSundeeHub.getPlugin(), new ProcessMenu(event.getPlayer()));
        } else {
            if (UtilityMethods.getSGShopChest() != null) {
                if (event.getClickedBlock().getLocation().equals(UtilityMethods.getSGShopChest().getLocation())) {
                    MenuTools.getNewMenuForState(MenuState.SG_SHOP).onOpen(event.getPlayer());
                    event.setCancelled(true);
                }
            }
        }
    }


    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (User.containsUser(event.getPlayer().getName())) {
            User user = User.getUser(event.getPlayer().getName());
            if (user.currentPet != null) {
                user.currentPet.remove();
            }
            user.removeUser(event.getPlayer().getName());
        }

        event.setQuitMessage(null);
    }

    @EventHandler(ignoreCancelled = true)
    public void onWeatherChange(WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (!UtilityMethods.canDropItems(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onInventoryClose(InventoryCloseEvent event) {
        User user = User.getUser(event.getPlayer().getName());
        if (user.getCurrentMenuState() != MenuState.NONE) {
            MenuTools.getNewMenuForState(user.getCurrentMenuState()).onClose((Player) event.getPlayer());
        }
    }

    @EventHandler(ignoreCancelled = false)
    public void onInventoryClick(InventoryClickEvent event) {

        User clicker = User.getUser(((Player) event.getWhoClicked()).getPlayer().getName());
        if (UtilityMethods.canDropItems((Player) event.getWhoClicked()) && clicker.getCurrentMenuState() == MenuState.NONE) {
            return;
        }
        if (clicker.getCurrentMenuState() != MenuState.NONE) {
            InventoryMenu menu = MenuTools.getNewMenuForState(clicker.getCurrentMenuState());
            if (menu != null) {
                menu.onClick(((Player) event.getWhoClicked()).getPlayer(), event.getCurrentItem(), event.getSlot());
            }
        } else if (event.getSlot() >= 9 && event.getSlot() != 39) {
            if (event.getCurrentItem() == event.getWhoClicked().getInventory().getHelmet()) {
                event.getWhoClicked().getInventory().setHelmet(new ItemStack(Material.AIR));
            } else {
                event.getWhoClicked().getInventory().setHelmet(event.getCurrentItem());
            }
            ProcessMenu.fillPlayerInventoryWithItems((Player) event.getWhoClicked());
        } else if (event.getSlot() == 39) {
            event.getWhoClicked().getInventory().setHelmet(new ItemStack(Material.AIR));
        }
        event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if (event.getEntityType().equals(EntityType.ENDER_PEARL)) {
            if (event.getEntity().getShooter() instanceof Player) {
                event.setCancelled(true);

                // Put the ender pearl back into the player's inventory
                final Player p = (Player) event.getEntity().getShooter();
                Bukkit.getScheduler().runTaskLater(SSundeeHub.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        ItemStack item = new ItemStack(Material.ENDER_PEARL);
                        ItemMeta teleportMeta = item.getItemMeta();
                        teleportMeta.setDisplayName("Teleport");
                        item.setItemMeta(teleportMeta);
                        p.getInventory().setItem(7, item);
                    }
                }, 1);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        User user = User.getUser(event.getPlayer().getName());
        if (user.isMuted() && user.getMuteTime() == 0) {
            event.getPlayer().sendMessage(ChatColor.RED + "You are muted and may not chat at this time.");
            event.setCancelled(true);
            return;
        }

        if (user.isMuted() && user.getMuteForTime() > System.currentTimeMillis()) {
            event.getPlayer().sendMessage(ChatColor.RED + "You are muted for " +
                    ((user.getMuteTime() - System.currentTimeMillis()) / 1000) + "more seconds");
            event.setCancelled(true);
            return;
        }

        if (user.isMuted() && user.getMuteForTime() <= System.currentTimeMillis()) {
            user.resetMuteTime();
        }

        if ((user.getLastMessageTime() > 10000) == false && user.getRank().equalsIgnoreCase("DEFAULT")) {
            event.getPlayer().sendMessage(ChatColor.RED + "You must wait " +
                    (10 - (user.getLastMessageTime() / 10000)) + " seconds before you can send another message");
            event.setCancelled(true);
            return;
        }

        String message = event.getMessage();
        if (SSundeeHub.getPerms().has(event.getPlayer(), "ssundeehub.bypasschatfilter") == false &&
                (failCharacterSpam(message) || failCurse(message) ||
                        failLink(message) || failIP(message) || failCaps(message))) {
            event.getPlayer().sendMessage(ChatColor.RED + "Your message looks like spam, please rephrase it.");
            event.setCancelled(true);
            return;
        }

        user.setLastMessageTime();
    }

    private static boolean failCharacterSpam(String message) {
        message = message.toLowerCase();

        int occurances = 0;
        char lastCharacter = 'a'; //Just start at a because I (Matt) is lazy.
        for (char curCharacter : message.toCharArray()) {
            if (curCharacter == lastCharacter) {
                occurances++;
            }
            lastCharacter = curCharacter;
        }

        double percentage = (double) occurances / message.length();
        return percentage >= 0.5;
    }

    private static boolean failCaps(String message) {
        if (message.length() > 3) {
            int letterCount = 0;
            int failedCount = 0;

            for (Character c : message.toCharArray()) {
                if (Character.isLetter(c)) {
                    letterCount++;
                    if (Character.isUpperCase(c)) {
                        failedCount++;
                    }
                }
            }

            double percentage = (double) failedCount / letterCount;
            return percentage >= 0.5;
        }

        return false;
    }

    private static boolean failCurse(String message) {
        message = message.toLowerCase();

        if (message.contains("fuck") || message.contains(" sucks ") || message.contains(" dick ") || message.contains(" shit ") || message.contains("nigger") || message.contains("[Owner]")) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean failLink(String message) {
        message = message.toLowerCase();

        if (message.contains("http") || message.contains(".com") || message.contains(".net") || message.contains(".org") || message.contains("www.")) {
            if (!(message.contains("mcprohosting.com") || message.contains("titanmc.net"))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static boolean failIP(String message) {
        message = message.toLowerCase();

        String IPADDRESS_PATTERN = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

        Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }
}
