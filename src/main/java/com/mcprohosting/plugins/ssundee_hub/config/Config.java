package com.mcprohosting.plugins.ssundee_hub.config;

import com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic.DynamicLeaderboardSign;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.Leaderboard;
import com.mcprohosting.plugins.ssundee_hub.utilities.map.ServerPortal;
import com.mcprohosting.plugins.ssundee_hub.utilities.map.ServerSign;
import com.mcprohosting.plugins.ssundee_hub.utilities.map.Spawn;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Config extends ConfigModel {

    public Config(Plugin plugin) {
        CONFIG_FILE = new File(plugin.getDataFolder(), "config.yml");
        CONFIG_HEADER = "SSundeeHub Configuration File";
    }

    public Spawn settings_spawn = new Spawn();
    public ArrayList<String> settings_message = new ArrayList<String>() {{
        add("&9Welcome!");
    }};
    public Map<String, ServerSign> serversigns = new HashMap<>();
    public Map<String, ServerPortal> serverportals = new HashMap<>();
    public ArrayList<DynamicLeaderboardSign> leaderboardsigns = new ArrayList<>();
    public Leaderboard sgleaderboard;
    public String database_host = "localhost";
    public int database_port = 3306;
    public String database_database = "minecraft";
    public String database_username = "admin";
    public String database_password = "1234";
    public String sgshopLocation = null;

    public void setSpawn(Location location) {
        settings_spawn = new Spawn(location);
    }
}
