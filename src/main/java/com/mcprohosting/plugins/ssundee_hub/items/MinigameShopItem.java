package com.mcprohosting.plugins.ssundee_hub.items;

import org.bukkit.Material;

public class MinigameShopItem {
    public int id;
    public int price;
    public Material type;
    public String name;
    public String description;
    public String game;

    public MinigameShopItem(int id, int price, Material type, String name, String description, String game) {
        this.id = id;
        this.price = price;
        this.type = type;
        this.name = name;
        this.description = description;
        this.game = game;
    }
}