package com.mcprohosting.plugins.ssundee_hub.items;

import com.mcprohosting.plugins.ssundee_hub.database.Database;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class HatItem {

    private static ArrayList<HatItem> currentHats;

    public static void refreshHats() {
        currentHats = DatabaseManager.getHatsFromDB();
    }

    public static ArrayList<HatItem> getServerHats() {
        return currentHats;
    }

    public String displayName;
    public int id;
    public int item;
    public int data;
    public String playerName;
    public int price;

    public HatItem(int id, int item, int data, String player, int price) {
        this.id = id;
        this.item = item;
        this.data = data;
        this.playerName = player;
        this.price = price;
    }

    public ItemStack toItemStack() {
        ItemStack thisHat;
        if (playerName == null) {
            thisHat = new ItemStack(this.item, 1, (short) 3, (byte) this.data);
        } else {
            thisHat = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        }
        ItemMeta thisHatMeta = thisHat.getItemMeta();
        ArrayList<String> thisHatLore = new ArrayList<String>();

        if (thisHat.getItemMeta() instanceof SkullMeta) {
            ((SkullMeta) thisHatMeta).setOwner(this.playerName);
            this.displayName = ChatColor.GOLD + playerName;
            thisHatMeta.setDisplayName(displayName);
        } else {
            this.displayName = ChatColor.GOLD + thisHat.getType().toString();
            thisHatMeta.setDisplayName(displayName);
        }

        thisHatLore.add(ChatColor.GREEN + "Price: " + this.price);
        thisHatMeta.setLore(thisHatLore);
        thisHat.setItemMeta(thisHatMeta);

        return thisHat;
    }

}
