package com.mcprohosting.plugins.ssundee_hub.database;

import com.gmail.favorlock.bonesqlib.MySQL;
import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;

import java.sql.SQLException;

public class Database {

    public static MySQL mysql;
    private String host;
    private int port;
    private String database;
    private String username;
    private String password;

    public Database() {
        init();
    }

    public static boolean isDBSchemaValid() {
        boolean retVal = false;
        retVal = mysql.isTable("network_users") &&
                mysql.isTable("network_pets") &&
                mysql.isTable("network_user_pets") &&
                mysql.isTable("network_hats") &&
                mysql.isTable("network_user_hats") &&
                mysql.isTable("network_achievements") &&
                mysql.isTable("network_user_achievements") &&
                mysql.isTable("network_user_friends") &&
                mysql.isTable("network_minigame_shop_items") &&
                mysql.isTable("network_user_minigame_shop_items") &&
                mysql.isTable("server_minigame_sg_status") &&
                mysql.isTable("server_minigame_sg_leaderboard") &&
                mysql.isTable("network_user_travel_powers");
        return retVal;
    }

    public static boolean setupTables() {
        boolean retVal = false;

        try {
            if (mysql.isTable("network_users") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_users` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `name` VARCHAR(16) NOT NULL," +
                        "  `tickets` INTEGER DEFAULT 0," +
                        "  `rank` TEXT NOT NULL," +
                        "  UNIQUE KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
            }
            if (mysql.isTable("network_pets") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_pets` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `entity` TEXT NOT NULL," +
                        "  `price` INTEGER NOT NULL," +
                        "  UNIQUE KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
            }
            if (mysql.isTable("network_user_pets") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_user_pets` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `petId` INTEGER NOT NULL," +
                        "  `customName` TEXT NOT NULL," +
                        "  `active` INTEGER NOT NULL," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)," +
                        "  FOREIGN KEY (`petId`)" +
                        "  REFERENCES `network_pets` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            if (mysql.isTable("network_hats") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_hats` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `item` INTEGER NOT NULL," +
                        "  `data` INTEGER NOT NULL," +
                        "  `player` VARCHAR(16) DEFAULT NULL," +
                        "  `price` INTEGER NOT NULL," +
                        "  UNIQUE KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
            }
            if (mysql.isTable("network_user_hats") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_user_hats` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `hatId` INTEGER NOT NULL," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)," +
                        "  FOREIGN KEY (`hatId`)" +
                        "  REFERENCES `network_hats` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            if (mysql.isTable("network_achievements") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_achievements` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `name` TEXT NOT NULL," +
                        "  `description` TEXT NOT NULL," +
                        "  `price` INTEGER NOT NULL," +
                        "  UNIQUE KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
            }
            if (mysql.isTable("network_user_achievements") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_user_achievements` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `achievementId` INTEGER NOT NULL," +
                        "  `progress` INTEGER DEFAULT 0," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)," +
                        "  FOREIGN KEY (`achievementId`)" +
                        "  REFERENCES `network_achievements` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            if (mysql.isTable("network_user_friends") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_user_friends` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `friendId` INTEGER NOT NULL," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)," +
                        "  FOREIGN KEY (`friendId`)" +
                        "  REFERENCES `network_users` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            if (mysql.isTable("network_minigame_shop_items") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_minigame_shop_items` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `item` INTEGER NOT NULL," +
                        "  `data` INTEGER NOT NULL," +
                        "  `game` TEXT NOT NULL," +
                        "  `name` TEXT NOT NULL," +
                        "  `description` TEXT NOT NULL," +
                        "  `action` TEXT NOT NULL," +
                        "  `price` INTEGER NOT NULL," +
                        "  UNIQUE KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
            }
            if (mysql.isTable("network_user_minigame_shop_items") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_user_minigame_shop_items` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `shopItemId` INTEGER NOT NULL," +
                        "  `itemQuantity` INTEGER NOT NULL," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)," +
                        "  FOREIGN KEY (`shopItemId`)" +
                        "  REFERENCES `network_minigame_shop_items` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            if (mysql.isTable("server_minigame_sg_status") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_status` (" +
                        "  `id` INTEGER NOT NULL AUTO_INCREMENT," +
                        "  `currentPlayers` INTEGER NOT NULL," +
                        "  `maxPlayers` INTEGER NOT NULL," +
                        "  `status` varchar(16) NOT NULL," +
                        "  UNIQUE KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
            }
            if (mysql.isTable("server_minigame_sg_leaderboard") == false) {
                mysql.query("CREATE TABLE IF NOT EXISTS `server_minigame_sg_leaderboard` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `wins` INTEGER NOT NULL," +
                        "  `totalGames` INTEGER NOT NULL," +
                        "  `mostKills` INTEGER NOT NULL," +
                        "  `totalKills` INTEGER NOT NULL," +
                        "  `totalDeaths` INTEGER NOT NULL," +
                        "  `totalPoints` INTEGER NOT NULL," +
                        "  `totalChests` INTEGER NOT NULL," +
                        "  `totalLife` INTEGER NOT NULL," +
                        "  UNIQUE KEY (`playerId`)," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            if (!mysql.isTable("network_user_travel_powers")) {
                mysql.query("CREATE TABLE IF NOT EXISTS `network_user_travel_powers` (" +
                        "  `playerId` INTEGER NOT NULL," +
                        "  `powerName` VARCHAR(16) NOT NULL," +
                        "  `powerLevel` INTEGER NOT NULL," +
                        "  PRIMARY KEY (`playerId`, `powerName`)," +
                        "  FOREIGN KEY (`playerId`)" +
                        "  REFERENCES `network_users` (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=latin1");
            }
            retVal = true;
        } catch (SQLException e) {
            try {
                if (mysql.isTable("network_users")) {
                    mysql.query("DROP TABLE network_users;");
                }
                if (mysql.isTable("network_pets")) {
                    mysql.query("DROP TABLE network_pets;");
                }
                if (mysql.isTable("network_user_pets")) {
                    mysql.query("DROP TABLE network_user_pets;");
                }
                if (mysql.isTable("network_hats")) {
                    mysql.query("DROP TABLE network_hats;");
                }
                if (mysql.isTable("network_user_hats")) {
                    mysql.query("DROP TABLE network_user_hats;");
                }
                if (mysql.isTable("network_achievements")) {
                    mysql.query("DROP TABLE network_achievements;");
                }
                if (mysql.isTable("network_user_achievements")) {
                    mysql.query("DROP TABLE network_user_achievements;");
                }
                if (mysql.isTable("network_friends")) {
                    mysql.query("DROP TABLE network_friends;");
                }
                if (mysql.isTable("network_minigame_shop_items")) {
                    mysql.query("DROP TABLE network_minigame_shop_items;");
                }
                if (mysql.isTable("network_user_minigame_shop_items")) {
                    mysql.query("DROP TABLE network_user_minigame_shop_items;");
                }
                if (mysql.isTable("server_minigame_sg_status")) {
                    mysql.query("DROP TABLE server_minigame_sg_status;");
                }
                if (mysql.isTable("server_minigame_sg_leaderboard")) {
                    mysql.query("DROP TABLE server_minigame_sg_leaderboard;");
                }
                if (mysql.isTable("network_user_travel_powers")) {
                    mysql.query("DROP TABLE network_user_travel_powers");
                }
            } catch (SQLException ex) {
                SSundeeHub.getPlugin().getLogger().severe("Error dropping tables!");
            }
            e.printStackTrace();
        }

        return retVal;
    }

    private void init() {
        loadConfig();
        mysql = new MySQL(SSundeeHub.getPlugin().getLogger(),
                "SssundeeHub",
                host,
                port,
                database,
                username,
                password);
        mysql.open();
    }

    private void loadConfig() {
        host = SSundeeHub.grabConfig().database_host;
        port = SSundeeHub.grabConfig().database_port;
        database = SSundeeHub.grabConfig().database_database;
        username = SSundeeHub.grabConfig().database_username;
        password = SSundeeHub.grabConfig().database_password;
    }
}
