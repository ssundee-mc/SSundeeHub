package com.mcprohosting.plugins.ssundee_hub.database;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.entities.Pet;
import com.mcprohosting.plugins.ssundee_hub.entities.PetType;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.SGLeaderboardStat;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerStatus;
import com.mcprohosting.plugins.ssundee_hub.items.MinigameShopItem;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DatabaseManager {

    private static Database database;

    public void init() {
        database = new Database();

        if (!(Database.setupTables() || Database.isDBSchemaValid())) {
            SSundeeHub.getPlugin().getLogger().info("Unable to create tables or validate schema. Plugin will not load until issue is fixed");
            Bukkit.getPluginManager().disablePlugin(SSundeeHub.getPlugin());
        }
    }

    public static Connection getConnection() {
        return getDatabase().mysql.getConnection();
    }

    public static void closeConnection(Connection connection) {
        Database.mysql.closeConnection(connection);
    }

    public static Database getDatabase() {
        return database;
    }

    public static ServerStatus getServerStatus(String prefix, int id) {
        ServerStatus status = null;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT currentPlayers, maxPlayers, status FROM server_minigame_" + prefix.toLowerCase() + "_status WHERE id = ?;");
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                status = new ServerStatus(prefix, id, rs.getInt(1), rs.getInt(2), rs.getString(3));
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to obtain the server status for " + prefix + id);
            closeConnection(connection);
        }

        return status;
    }

    public static ArrayList<SGLeaderboardStat> getTopEight() {
        ArrayList<SGLeaderboardStat> data = new ArrayList<SGLeaderboardStat>();

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT name, totalPoints, totalGames, wins, totalKills, totalDeaths FROM server_minigame_sg_leaderboard JOIN network_users ON id=playerId ORDER BY totalPoints DESC limit 8;");
            rs = ps.executeQuery();

            while (rs.next()) {
                data.add(new SGLeaderboardStat(rs));
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to obtain top 8 players in the leaderboard!");
            closeConnection(connection);
        }

        return data;
    }

    public static SGLeaderboardStat getPlayerLeadboardStats(int id) {
        SGLeaderboardStat stat = null;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT name, totalPoints, totalGames, wins, totalKills, totalDeaths FROM server_minigame_sg_leaderboard JOIN network_users ON id=playerId where playerId = ?;");
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                stat = new SGLeaderboardStat(rs);
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to obtain a players leaderboard stats!");
            closeConnection(connection);
        }

        return stat;
    }

    public static boolean containsPlayer(String name) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement("SELECT LOWER(name) FROM network_users WHERE name = LOWER(?);");
            ps.setString(1, name);
            rs = ps.executeQuery();

            retVal = rs.next();

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to check if a player exists in the database!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static boolean addPlayer(String name) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("INSERT INTO network_users (id, name, tickets, rank) VALUES (null, ?, 0, 'default');");
            ps.setString(1, name);
            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to add a player to the database!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static String[] getPlayerData(String name) {
        String[] retVal = new String[3];

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareCall("SELECT id, tickets, rank FROM network_users WHERE LOWER(name) = LOWER(?);");
            ps.setString(1, name);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                retVal[0] = rs.getInt("id") + "";
                retVal[1] = rs.getInt("tickets") + "";
                retVal[2] = rs.getString("rank");
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to get a users data from the database!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static String getRank(String name) {
        String retVal = "";

        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareCall("SELECT rank FROM network_users WHERE LOWER(name) = LOWER(?);");
            ps.setString(1, name);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                retVal = rs.getString(1);
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to get a users rank from the database!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static boolean updateRank(String name, String rank) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE network_users SET rank = LOWER(?) WHERE LOWER(name) = LOWER(?);");
            ps.setString(1, rank.toString());
            ps.setString(2, name);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to update a players rank in the database!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static boolean addTickets(String name, int tickets) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE network_users SET tickets = tickets + ? WHERE LOWER(name) = LOWER(?);");
            ps.setInt(1, tickets);
            ps.setString(2, name);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to update a players tickets in the database!");
            closeConnection(connection);
        }

        return retVal;
    }


    public static boolean removeTickets(String name, int tickets) {
        boolean retVal = false;

        Connection connection = getConnection();
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("UPDATE network_users SET tickets = tickets - ? WHERE LOWER(name) = LOWER(?);");
            ps.setInt(1, tickets);
            ps.setString(2, name);

            retVal = ps.executeUpdate() > 0;

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to update a players tickets in the database!");
            closeConnection(connection);
        }

        return retVal;
    }

    public static int getTickets(String name) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int retVal = -1;

        try {
            ps = connection.prepareCall("SELECT tickets FROM network_users WHERE LOWER(name) = LOWER(?);");
            ps.setString(1, name);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                retVal = rs.getInt(1);
            }

            ps.close();
            rs.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Failed to fetch ticket count from the database!");
            closeConnection(connection);
        }
        return retVal;
    }

    public static List<MinigameShopItem> getMinigameShopItemsFromDatabase(String gameID) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MinigameShopItem> items = new ArrayList<MinigameShopItem>();

        try {
            ps = connection.prepareCall("SELECT * from network_minigame_shop_items WHERE LOWER(game) = LOWER(?);");
            ps.setString(1, gameID);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                items.add(new MinigameShopItem(rs.getInt(1), rs.getInt(8), Material.getMaterial(rs.getInt(2)),
                        rs.getString(5), rs.getString(6), rs.getString(4)));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            SSundeeHub.getPlugin().getLogger().info("Unable to load store items over the DB!");
            closeConnection(connection);
        }
        closeConnection(connection);
        return items;
    }


    public static ArrayList<MinigameShopItem> getUserItemsFromDatabase(int playerID, String gameID) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MinigameShopItem> items = new ArrayList<MinigameShopItem>();

        try {
            ps = connection.prepareCall("SELECT shopItemId from network_user_minigame_shop_items WHERE playerId = ?");
            ps.setInt(1, playerID);
            rs = ps.executeQuery();
            List<MinigameShopItem> itemsToChoose = getMinigameShopItemsFromDatabase(gameID);
            while (rs.next() != false) {
                for (MinigameShopItem i : itemsToChoose) {
                    if (i.id == rs.getInt(1)) {
                        items.add(i);
                    }
                }
            }
        } catch (SQLException ex) {
            SSundeeHub.getPlugin().getLogger().info("Unable to load player items over the DB!");
            closeConnection(connection);
        }
        closeConnection(connection);
        return items;
    }

    public static boolean addItemForPlayer(int itemId, int playerId) {
        boolean retCode = false;
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            if (getItemQuantityForItem(itemId, playerId) <= 0) {
                ps = connection.prepareStatement("INSERT INTO network_user_minigame_shop_items (playerId, shopItemId, itemQuantity) VALUES (?,?,?);");
                ps.setInt(1, playerId);
                ps.setInt(2, itemId);
                ps.setInt(3, 1);
            } else {
                ps = connection.prepareStatement("UPDATE network_user_minigame_shop_items SET itemQuantity = itemQuantity + 1 WHERE playerId = ? AND shopItemId = ?;");
                ps.setInt(1, playerId);
                ps.setInt(2, itemId);
            }


            retCode = ps.executeUpdate() > 0;

            ps.close();
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to update item for a player!!");
            e.printStackTrace();
            closeConnection(connection);
        }

        closeConnection(connection);
        return retCode;
    }

    public static int getItemQuantityForItem(int itemId, int playerId) {
        int quantity = 0;
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareCall("SELECT itemQuantity FROM network_user_minigame_shop_items WHERE playerId = ? AND shopItemId = ?;");
            ps.setInt(1, playerId);
            ps.setInt(2, itemId);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                quantity = rs.getInt(1);
            }
            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to get item quanitity!");
            closeConnection(connection);
        }

        return quantity;
    }

    public static void addItemToShopDB(int materialId, String game, String name, String description, String action, int price) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareCall("INSERT INTO network_minigame_shop_items (item, data, game, name, description, action, price) VALUES (?,0,?,?,?,?,?);");
            ps.setInt(1, materialId);
            ps.setString(2, game);
            ps.setString(3, name);
            ps.setString(4, description);
            ps.setString(5, action);
            ps.setInt(6, price);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            SSundeeHub.getPlugin().getLogger().info("Unable to add item to item shop!");
            closeConnection(connection);
        }

        closeConnection(connection);
    }

    public static ArrayList<HatItem> getHatsFromDB() {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<HatItem> hats = new ArrayList<HatItem>();
        try {
            ps = connection.prepareStatement("SELECT * FROM network_hats");
            rs = ps.executeQuery();

            while (rs.next() != false) {
                hats.add(new HatItem(rs.getInt("id"), rs.getInt("item"), rs.getInt("data"),
                        rs.getString("player"), rs.getInt("price")));
            }
            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to pull hats from DB!");
            e.printStackTrace();
            closeConnection(connection);
        }

        return hats;
    }

    public static HatItem getHatByID(int hatId) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        HatItem hat = null;
        try {
            ps = connection.prepareStatement("SELECT * FROM network_hats WHERE id = ?");
            ps.setInt(1, hatId);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                hat = new HatItem(rs.getInt("id"), rs.getInt("item"), rs.getInt("data"),
                        rs.getString("player"), rs.getInt("price"));
            }
            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to get hat by ID!");
            e.printStackTrace();
            closeConnection(connection);
        }

        return hat;
    }

    public static void addHatForUser(int playerId, int hatId) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareCall("INSERT INTO network_user_hats (playerId, hatId) VALUES (?,?);");
            ps.setInt(1, playerId);
            ps.setInt(2, hatId);
            ps.executeUpdate();

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to add a hat for a player!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static void addHatToDB(int price, String owner) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareCall("INSERT INTO network_hats (item, data, player, price) VALUES (?,?,?,?);");
            ps.setInt(1, Material.SKULL_ITEM.getId());
            ps.setInt(2, 0);
            ps.setString(3, owner);
            ps.setInt(4, price);
            ps.executeUpdate();

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to add a hat to the DB!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static void addHatToDB(int price, int item, int data) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareCall("INSERT INTO network_hats (item, data, player, price) VALUES (?,?,?,?);");
            ps.setInt(1, item);
            ps.setInt(2, data);
            ps.setString(3, null);
            ps.setInt(4, price);
            ps.executeUpdate();

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to add a hat to the DB!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static ArrayList<HatItem> getHatsForUser(int userId) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<HatItem> hats = new ArrayList<HatItem>();
        try {
            ps = connection.prepareStatement("SELECT hatId FROM network_user_hats WHERE playerId = ?");
            ps.setInt(1, userId);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                hats.add(getHatByID(rs.getInt(1)));
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to get hats for a user!");
            e.printStackTrace();
            closeConnection(connection);
        }

        return hats;
    }

    public static void removeItemFromDB(MinigameShopItem i) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Integer> userIds = new ArrayList<Integer>();
        try {
            ps = connection.prepareStatement("SELECT playerId FROM network_user_minigame_shop_items WHERE shopItemId = ?");
            ps.setInt(1, i.id);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                userIds.add(rs.getInt("playerId"));
            }
            rs.close();
            ps = connection.prepareCall("DELETE FROM network_user_minigame_shop_items WHERE shopItemId = ?");
            ps.setInt(1, i.id);
            ps.executeUpdate();
            ps = connection.prepareCall("DELETE FROM network_minigame_shop_items WHERE id = ?");
            ps.setInt(1, i.id);
            ps.executeUpdate();


            for (Integer integer : userIds) {
                ps = connection.prepareStatement("UPDATE network_users SET tickets = tickets + ? WHERE id = ?");
                ps.setInt(1, i.price);
                ps.setInt(2, integer);
                ps.executeUpdate();
            }

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to remove item from DB!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static void removeHatFromDB(HatItem hat) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Integer> userIds = new ArrayList<Integer>();
        try {
            ps = connection.prepareStatement("SELECT playerId FROM network_user_hats WHERE hatId = ?");
            ps.setInt(1, hat.id);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                userIds.add(rs.getInt("playerId"));
            }
            rs.close();
            ps = connection.prepareCall("DELETE FROM network_user_hats WHERE hatId = ?");
            ps.setInt(1, hat.id);
            ps.executeUpdate();
            ps = connection.prepareCall("DELETE FROM network_hats WHERE id = ?");
            ps.setInt(1, hat.id);
            ps.executeUpdate();

            for (Integer i : userIds) {
                ps = connection.prepareStatement("UPDATE network_users SET tickets = tickets + ? WHERE id = ?");
                ps.setInt(1, hat.price);
                ps.setInt(2, i);
                ps.executeUpdate();
            }

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to remove item from DB!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static HashMap<TravelPower, Integer> getTravelPowersForUser(int userId) {
        Connection connection = getConnection();
        PreparedStatement ps;
        ResultSet rs;
        HashMap<TravelPower, Integer> powers = new HashMap<>();

        try {
            ps = connection.prepareStatement("SELECT powerName, powerLevel FROM network_user_travel_powers WHERE playerId = ?");
            ps.setInt(1, userId);
            rs = ps.executeQuery();

            while (rs.next()) {
                powers.put(TravelPower.valueOf(rs.getString("powerName")), rs.getInt("powerLevel"));
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to retrieve travel powers for a user!");
            e.printStackTrace();
            closeConnection(connection);
        }

        return powers;
    }

    public static void updateTravelPowerForUser(int userId, TravelPower power, int powerLevel) {
        Connection connection = getConnection();
        PreparedStatement ps;

        try {
            ps = connection.prepareStatement("INSERT INTO network_user_travel_powers VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE powerLevel = ?");
            ps.setInt(1, userId);
            ps.setString(2, power.toString());
            ps.setInt(3, powerLevel);
            ps.setInt(4, powerLevel);
            ps.executeUpdate();

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to update a travel power for a user!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    // -- PETS --
    public static ArrayList<Pet> getPetsForSale() {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Pet> pets = new ArrayList<Pet>();

        try {
            ps = connection.prepareStatement("SELECT * FROM network_pets");
            rs = ps.executeQuery();

            while (rs.next() != false) {
                pets.add(new Pet(PetType.getPetTypeForName(rs.getString(2)), rs.getInt(1), rs.getInt(3)));
            }

            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to get pets from DB!");
            e.printStackTrace();
            closeConnection(connection);
        }

        return pets;
    }


    public static ArrayList<Pet> getPetsOwnedForPlayer(Player p) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Pet> pets = new ArrayList<Pet>();
        try {
            ps = connection.prepareStatement("SELECT * FROM network_user_pets WHERE playerId = ?");
            ps.setInt(1, User.getUser(p.getName()).getId());
            rs = ps.executeQuery();


            while (rs.next() != false) {
                Pet petKind = Pet.getPetById(rs.getInt(2));
                if (p != null) {
                    pets.add(new Pet(petKind.type, petKind.id, p, rs.getString(3)));
                }
            }
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to get pets for user!");
            e.printStackTrace();
            closeConnection(connection);
        }
        return pets;
    }

    public static Pet getActivePetForPlayer(Player p) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Pet pet = null;

        try {
            ps = connection.prepareStatement("SELECT * FROM network_user_pets WHERE playerId = ? AND active = 1");
            ps.setInt(1, User.getUser(p.getName()).getId());
            rs = ps.executeQuery();

            while (rs.next() != false) {
                Pet petKind = Pet.getPetById(rs.getInt(2));
                if (petKind != null) {
                    pet = new Pet(petKind.type, petKind.id, p, rs.getString(3));
                }
            }
            rs.close();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to get active player ID!");
            e.printStackTrace();
            closeConnection(connection);
        }

        return pet;
    }

    public static void addPetForPlayer(int playerId, int petId) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {

            ps = connection.prepareCall("INSERT INTO network_user_pets (playerId, petId, customName, active) VALUES (?,?,?,0)");
            ps.setInt(1, playerId);
            ps.setInt(2, petId);
            ps.setString(3, "DEFAULT");
            ps.executeUpdate();
            ps.close();
            closeConnection(connection);


        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to add pet for player!");
            e.printStackTrace();
            closeConnection(connection);
        }

    }

    public static void setActivePetForPlayer(int playerId, int petId) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareCall("UPDATE network_user_pets SET active = 0 WHERE playerId = ?");
            ps.setInt(1, playerId);
            ps.executeUpdate();
            if (petId != -1) {
                ps = connection.prepareCall("UPDATE network_user_pets SET active = 1 WHERE playerId = ? AND petId = ?");
                ps.setInt(1, playerId);
                ps.setInt(2, petId);
                ps.executeUpdate();
            }

            ps.close();
            closeConnection(connection);

        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to set a player's active pet!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static void updatePetNameFor(int playerId, int petId, String name) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareCall("UPDATE network_user_pets SET customName = ? WHERE playerId = ? AND petId = ?");
            ps.setString(1, name);
            ps.setInt(2, playerId);
            ps.setInt(3, petId);
            ps.executeUpdate();

            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to update pet name!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static void addPetToDB(PetType t, int price) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareCall("INSERT INTO network_pets (entity, price) VALUES (?,?)");
            ps.setString(1, t.toString());
            ps.setInt(2, price);
            ps.executeUpdate();
            ps.close();
            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to add a new pet to the DB!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }

    public static void removePetFromDB(Pet pet) {
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Integer> ids = new ArrayList<Integer>();
        try {
            ps = connection.prepareStatement("SELECT playerId FROM network_user_pets WHERE petId = ?");
            ps.setInt(1, pet.id);
            rs = ps.executeQuery();

            while (rs.next() != false) {
                ids.add(rs.getInt("playerId"));
            }
            rs.close();
            ps = connection.prepareStatement("DELETE FROM network_user_pets WHERE petId = ?");
            ps.setInt(1, pet.id);
            ps.executeUpdate();
            ps = connection.prepareStatement("DELETE FROM network_pets WHERE id = ?");
            ps.setInt(1, pet.id);
            ps.executeUpdate();
            for (Integer i : ids) {
                ps = connection.prepareStatement("UPDATE network_users SET tickets = tickets + ? WHERE id = ?");
                ps.setInt(1, pet.price);
                ps.setInt(2, i);
                ps.executeUpdate();
            }

            ps.close();

            closeConnection(connection);
        } catch (SQLException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to delete pet from DB!");
            e.printStackTrace();
            closeConnection(connection);
        }
    }


    // -- END PETS --
}
