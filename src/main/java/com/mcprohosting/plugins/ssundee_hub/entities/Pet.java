package com.mcprohosting.plugins.ssundee_hub.entities;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class Pet {

    private static ArrayList<Pet> pets;

    public PetType type;
    public int id;
    public int price;
    public Player owner;
    public String name;

    public Pet(PetType type, int id, int price) {
        this.type = type;
        this.id = id;
        this.price = price;
        owner = null;
    }

    public Pet(PetType type, int id, Player owner, String name) {
        this.type = type;
        this.id = id;
        this.owner = owner;
        this.name = name;
    }

    public Entity spawnPet() {
        if (this.owner == null) {
            return null;
        }
        switch (this.type) {
            default:
                return null;
            case WOLF:
                Wolf pet = owner.getLocation().getWorld().spawn(owner.getLocation(), Wolf.class);
                pet.setOwner(owner);
                if (this.name.equalsIgnoreCase("default")) {
                    pet.setCustomName(this.owner.getName() + "'s Wolf");
                    pet.setCustomNameVisible(true);
                } else {
                    pet.setCustomName(this.owner.getName() + "'s " + this.name);
                    pet.setCustomNameVisible(true);
                }
                return pet;
            case OCELOT:
                Ocelot oPet = owner.getLocation().getWorld().spawn(owner.getLocation(), Ocelot.class);
                oPet.setOwner(owner);
                if (this.name.equalsIgnoreCase("default")) {
                    oPet.setCustomName(this.owner.getName() + "'s Ocelot");
                    oPet.setCustomNameVisible(true);
                } else {
                    oPet.setCustomName(this.owner.getName() + "'s " + this.name);
                    oPet.setCustomNameVisible(true);
                }
                return oPet;

        }
    }

    public ItemStack toItemStack() {
        if (this.owner != null) {
            return null;
        }
        switch (this.type) {
            default:
                return null;
            case WOLF:
                ItemStack bone = new ItemStack(Material.BONE);
                ItemMeta boneMeta = bone.getItemMeta();
                boneMeta.setDisplayName("Wolf Pet");
                ArrayList<String> boneLore = new ArrayList<String>();
                boneLore.add("A friendly wolf that follows you around.");
                boneLore.add(this.price + " tickets.");
                boneMeta.setLore(boneLore);
                bone.setItemMeta(boneMeta);
                return bone;
            case OCELOT:
                ItemStack fish = new ItemStack(Material.COOKED_FISH);
                ItemMeta fishMeta = fish.getItemMeta();
                fishMeta.setDisplayName("Ocelot Pet");
                ArrayList<String> fishLore = new ArrayList<String>();
                fishLore.add("A cute ocelot that follows you around.");
                fishLore.add(this.price + " tickets.");
                fishMeta.setLore(fishLore);
                fish.setItemMeta(fishMeta);
                return fish;
        }
    }

    public static void refreshPets() {
        pets = DatabaseManager.getPetsForSale();
    }

    public static Pet getPetById(int id) {
        Pet p = null;
        for (Pet pet : pets) {
            if (pet.id == id) {
                p = pet;
            }
        }
        return p;
    }

    public static ArrayList<Pet> getPets() {
        return pets;
    }

}
