package com.mcprohosting.plugins.ssundee_hub.entities;

public enum PetType {
    INVALID("invalid"),
    WOLF("wolf"),
    OCELOT("ocelot");

    private String value;

    PetType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }

    public static PetType getPetTypeForName(String name) {
        if (name.equalsIgnoreCase("wolf")) {
            return WOLF;
        }
        if (name.equalsIgnoreCase("ocelot")) {
            return OCELOT;
        }

        return INVALID;
    }
}
