package com.mcprohosting.plugins.ssundee_hub.entities;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.commands.ShutUp;
import com.mcprohosting.plugins.ssundee_hub.database.Database;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerPortals;
import com.mcprohosting.plugins.ssundee_hub.utilities.map.ServerPortal;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.items.MinigameShopItem;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

public class User {

    // Static User Map
    private static Map<String, User> users;

    // User Data
    private final String name;
    private int id;
    private String rank;
    private int tickets = 0;

    // Chat Data
    private boolean muted = false;
    private long lastMessageTime = 0;
    private int muteTime = 0;
    private long muteStart = 0;

    // Editing Portals
    private ServerPortal portal;
    private String destination;
    private Block block;

    // Portal Travel
    private long lastTraveling = 0;

    // Menu Data
    private MenuState currentMenuState;
    private int vanishState = 0;
    private long lastVanishClick = 0;

    // SGS Inventory
    ArrayList<MinigameShopItem> userItems = new ArrayList<MinigameShopItem>();

    // Travel Powers
    private HashMap<TravelPower, Integer> travelPowers;
    private HashMap<TravelPower, Integer> ownedLevels;
    private boolean speedPowerActive = false;
    private boolean jumpPowerActive = false;
    private boolean flyPowerActive = false;
    private long lastTeleportClick = 0;

    public Entity currentPet = null;

    static {
        users = new HashMap<String, User>();
    }

    public User(String name) {
        this.name = name;
        init();
    }

    public void init() {
        String[] data = DatabaseManager.getPlayerData(this.name);
        this.id = Integer.parseInt(data[0]);
        this.tickets = getTicketsFromString(data[1]);
        this.rank = data[2];
        updateTravelPowers();
    }

    public int getId() {
        return id;
    }

    public void setEditingPortal(String destination) {
        this.destination = destination;
        this.portal = null;
    }

    public void updatePortal(Block block) {
        if (this.block == null) {
            this.block = block;
        } else {
            portal = new ServerPortal(this.block.getLocation(), block.getLocation());
            finishEditing();
        }
    }

    public void finishEditing() {
        ServerPortals.endPlayerEditing(name);
        ServerPortals.addPortal(destination, portal);
        destination = null;
        portal = null;
        block = null;
    }

    public long getLastTraveling() {
        return lastTraveling;
    }

    public void setLastTraveling(long lastTraveling) {
        this.lastTraveling = lastTraveling;
    }

    public long getLastVanishClick() {
        return lastVanishClick;
    }

    public void setLastVanishClick(long lastVanishClick) {
        this.lastVanishClick = lastVanishClick;
    }

    public void addUser(String name) {
        users.put(name, this);
    }

    public void removeUser(String name) {
        users.remove(name);
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public boolean isMuted() {
        return muted;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    public int getMuteTime() {
        return muteTime;
    }

    public long getMuteForTime() {
        return muteTime * 1000 * 60 + System.currentTimeMillis();
    }

    public void setMuteTime(int muteTime) {
        this.muteTime = muteTime;
        this.muteStart = System.currentTimeMillis() + (1000 * 60 * muteTime);

        setMuted(true);
    }

    public void resetMuteTime() {
        muteTime = 0;
        muteStart = 0;

        if (ShutUp.getMuted() == false) {
            setMuted(false);
        }
    }

    public long getLastMessageTime() {
        return (System.currentTimeMillis() - lastMessageTime);
    }

    public void setLastMessageTime() {
        lastMessageTime = System.currentTimeMillis();
    }

    public void setMenuState(MenuState state) {
        this.currentMenuState = state;
    }

    public MenuState getCurrentMenuState() {
        return currentMenuState;
    }

    public int getTickets() {
        return tickets;
    }

    private int getTicketsFromString(String data) {
        int tickets = Integer.parseInt(data);
        if (tickets == -1)
            return 0;
        else
            return tickets;
    }

    public void setTickets(int tickets) {
        this.tickets = tickets;
    }

    public int getVanishState() {
        return vanishState;
    }

    public void setVanishState() {
        switch (vanishState) {
            case 0:
                this.vanishState = 1;
                return;
            case 1:
                this.vanishState = 2;
                return;
            case 2:
                this.vanishState = 0;
                return;
        }
    }

    public int getTravelPowerLevel(TravelPower power) {
        return travelPowers.containsKey(power) ? travelPowers.get(power) : 0;
    }

    public void setTravelPowerLevel(TravelPower power, int level, boolean owned) {
        travelPowers.put(power, level);
        if (owned == false) {
            DatabaseManager.updateTravelPowerForUser(getId(), power, level);
        }
    }

    public void updateTravelPower(TravelPower power, boolean toggle) {
        Player player = Bukkit.getPlayer(getName());
        if (player == null || !player.isOnline()) {
            return;
        }

        if (flyPowerActive == false) {
            player.setAllowFlight(false);
        }

        switch (power) {
            case SPEED:
                if (toggle) {
                    speedPowerActive = !speedPowerActive;

                    if (speedPowerActive) {
                        if (jumpPowerActive) {
                            updateTravelPower(TravelPower.JUMP, true);
                        }
                        if (flyPowerActive) {
                            updateTravelPower(TravelPower.FLY, true);
                        }
                    }
                }

                if (speedPowerActive) {
                    int level = getTravelPowerLevel(TravelPower.SPEED);
                    float speed = 0.2f + (level * (0.8f / 9.0f)); // Normal walk speed is 0.2, max is 1.0, speed increases linearly with each level increase
                    if (speed > 1.0f) {
                        speed = 1.0f;
                    }
                    player.setWalkSpeed(speed);
                } else {
                    player.setWalkSpeed(0.2f);
                }

                break;
            case JUMP:
                if (toggle) {
                    jumpPowerActive = !jumpPowerActive;

                    if (jumpPowerActive) {
                        if (speedPowerActive) {
                            updateTravelPower(TravelPower.SPEED, true);
                        }
                        if (flyPowerActive) {
                            updateTravelPower(TravelPower.FLY, true);
                        }
                    }
                }

                if (jumpPowerActive) {
                    int level = getTravelPowerLevel(TravelPower.JUMP);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, level - 1, true), true);

                    if (ownedLevels.containsKey(TravelPower.DOUBLE_JUMP)) {
                        if (ownedLevels.get(TravelPower.DOUBLE_JUMP) == 1 && ownedLevels.get(TravelPower.JUMP) >= 5) {
                            travelPowers.put(TravelPower.DOUBLE_JUMP, 1);
                        }
                    }
                } else {
                    player.removePotionEffect(PotionEffectType.JUMP);
                    travelPowers.put(TravelPower.DOUBLE_JUMP, 0);
                }
                break;
            case FLY:
                if (toggle) {
                    flyPowerActive = !flyPowerActive;
                    player.setAllowFlight(flyPowerActive);

                    if (flyPowerActive) {
                        if (jumpPowerActive) {
                            updateTravelPower(TravelPower.JUMP, true);
                        }
                        if (speedPowerActive) {
                            updateTravelPower(TravelPower.SPEED, true);
                        }
                    }
                }

                if (flyPowerActive) {
                    int level = getTravelPowerLevel(TravelPower.FLY);
                    float speed = 0.1f * (level + 1); // Normal fly speed is 0.1, max is 1.0, speed increases linearly with each level increase
                    if (speed > 1.0f) {
                        speed = 1.0f;
                    }
                    player.setFlySpeed(speed);
                }
                break;
            default:
                break;
        }
    }

    public boolean isTravelPowerActive(TravelPower power) {
        Integer val;

        switch (power) {
            case SPEED:
                return speedPowerActive;
            case JUMP:
                return jumpPowerActive;
            case TELEPORT:
                val = travelPowers.get(TravelPower.TELEPORT);
                return val != null && val > 0;
            case FLY:
                return flyPowerActive;
            case DOUBLE_JUMP:
                val = travelPowers.get(TravelPower.DOUBLE_JUMP);
                return val != null && val == 1;
            case TP_LOCATION:
                val = travelPowers.get(TravelPower.TP_LOCATION);
                return val != null && val == 1;
            case TP_PlAYER:
                val = travelPowers.get(TravelPower.TP_PlAYER);
                return val != null && val == 1;
            default:
                return false;
        }
    }

    public int getOwnedTravelPowerLevel(TravelPower power) {
        return ownedLevels.containsKey(power) ? ownedLevels.get(power) : 0;
    }

    public void updateTravelPowers() {
        Player player;

        this.travelPowers = DatabaseManager.getTravelPowersForUser(id);
        this.ownedLevels = DatabaseManager.getTravelPowersForUser(id);

        travelPowers.put(TravelPower.DOUBLE_JUMP, 0);

        if ((player = Bukkit.getPlayer(getName())) == null) {
            return;
        }

        player.setWalkSpeed(0.2f);
        player.removePotionEffect(PotionEffectType.JUMP);
        player.setAllowFlight(false);
    }

    public void refreshOwnedTravelPowers() {
        this.ownedLevels = DatabaseManager.getTravelPowersForUser(id);
    }

    public void setLastTeleportClick(long ms) {
        this.lastTeleportClick = ms;
    }

    public long getLastTeleportClick() {
        return this.lastTeleportClick;
    }

    public String getName() {
        return this.name;
    }

    public static User getUser(String name) {
        return users.get(name);
    }

    public static Collection<User> getUsers() {
        return users.values();
    }

    public static boolean containsUser(String name) {
        if (users.containsKey(name)) {
            return true;
        }

        return false;
    }


}
