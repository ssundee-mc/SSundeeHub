package com.mcprohosting.plugins.ssundee_hub;

import com.mcprohosting.plugins.ssundee_hub.commands.*;
import com.mcprohosting.plugins.ssundee_hub.config.Config;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.Pet;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic.DynamicLeaderboardSigns;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.UpdateSGLeaderboard;
import com.mcprohosting.plugins.ssundee_hub.listeners.*;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.SGLeaderboard;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerPortals;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerSigns;
import com.mcprohosting.plugins.ssundee_hub.utilities.UpdateStatusSigns;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Wolf;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class SSundeeHub extends JavaPlugin {
    private static Plugin plugin;
    private static Config config;
    private static DatabaseManager databaseManager;
    private static Permission perms;
    private static Chat chat;

    public void onEnable() {
        // Allow static access to the instance of this plugin
        plugin = this;

        // Initialize the Configuration object
        config = new Config(this);
        try {
            config.init();
        } catch (InvalidConfigurationException e) {
            getLogger().warning("Failed to initialize the config!");
        }

        // Initialize the database
        databaseManager = new DatabaseManager();
        databaseManager.init();

        // Initialize the server signs
        ServerSigns.init();

        // Initialize the server portals
        ServerPortals.init();

        // Initilize dynamic leaderboard signs
        DynamicLeaderboardSigns.init();

        // Initialize the leaderboard signs
        new SGLeaderboard();

        // Register command executors
        registerCommands();

        // Register event listeners
        registerListeners();

        // Register plugin message channels
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        // Schedule sign updater
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new UpdateStatusSigns(), 20, 20);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new UpdateSGLeaderboard(), 20, 20 * 60);

        // Load the current hat and item data into memory.
        HatItem.refreshHats();
        Pet.refreshPets();

        // Initialize permissions hook
        setupPermissions();

        // Initialize chat hook
        setupChat();

        // Remove any pets that are left over from a restart.
        for (Entity entity : Bukkit.getWorlds().get(0).getEntitiesByClasses(Wolf.class, Ocelot.class)) {
            if (((Tameable) entity).isTamed()) {
                entity.remove();
            }
        }
    }

    public void onDisable() {
        try {
            grabConfig().save();
        } catch (InvalidConfigurationException e) {
            getLogger().warning("Failed to save the config!");
        }
    }

    public static Plugin getPlugin() {
        return plugin;
    }

    public static Config grabConfig() {
        return config;
    }

    public static void saveConf() {
        try {
            config.save();
        } catch (InvalidConfigurationException e) {
            SSundeeHub.getPlugin().getLogger().warning("Failed to save the config!");
        }
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            perms = permissionProvider.getProvider();
        }
        return (perms != null);
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    public static Permission getPerms() {
        return perms;
    }

    public static Chat getChat() {
        return chat;
    }

    private void registerCommands() {
        getCommand("setspawn").setExecutor(new SetSpawn());
        getCommand("setsign").setExecutor(new SetSign());
        getCommand("setportal").setExecutor(new SetPortal());
        getCommand("sglbsigns").setExecutor(new SetSGLeaderboard());
        getCommand("giverank").setExecutor(new SetRank());
        getCommand("addtickets").setExecutor(new AddTickets());
        getCommand("setsgshop").setExecutor(new SetSGShop());
        getCommand("shutup").setExecutor(new ShutUp());
        getCommand("muteplayer").setExecutor(new MutePlayer());
        getCommand("minigameItems").setExecutor(new MinigameItems());
        getCommand("stats").setExecutor(new Stats());
        getCommand("addsgitem").setExecutor(new NewSGItem());
        getCommand("removesgitem").setExecutor(new RemoveSGItem());
        getCommand("addhat").setExecutor(new AddHat());
        getCommand("removehat").setExecutor(new RemoveHat());
        getCommand("addpet").setExecutor(new AddPet());
        getCommand("removepet").setExecutor(new RemovePet());
        getCommand("hubversion").setExecutor(new Version());
        getCommand("setpetname").setExecutor(new SetPetName());
    }

    private void registerListeners() {
        getServer().getPluginManager().registerEvents(new DoubleJumpListener(), this);
        getServer().getPluginManager().registerEvents(new GeneralListeners(), this);
        getServer().getPluginManager().registerEvents(new PortalListeners(), this);
        getServer().getPluginManager().registerEvents(new SignListeners(), this);
        getServer().getPluginManager().registerEvents(new WorldListener(), this);
    }
}
