package com.mcprohosting.plugins.ssundee_hub.servers;

public class ServerStatus {

    private String prefix;
    private int id;
    private int currentPlayers;
    private int maxPlayers;
    private String status;

    public ServerStatus(String prefix, int id, int currentPlayers, int maxPlayers, String status) {
        this.prefix = prefix;
        this.id = id;
        this.currentPlayers = currentPlayers;
        this.maxPlayers = maxPlayers;
        this.status = status;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getId() {
        return id;
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public String getStatus() {
        return status;
    }

}
