package com.mcprohosting.plugins.ssundee_hub.servers;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.utilities.map.ServerSign;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ServerSigns {
    private static Map<String, ServerSign> signs = null;
    private static Map<String, String[]> playersEditingSigns = new HashMap<>();

    // Load all previously created server signs
    public static void init() {
        signs = SSundeeHub.grabConfig().serversigns;
    }

    public static void addSign(Block block, String gameName, String serverName) {

        signs.put(serverName, new ServerSign(block.getLocation()));
        SSundeeHub.saveConf();

        if (block.getState() instanceof Sign) {
            Sign sign = (Sign) block.getState();
            sign.setLine(0, gameName);
            sign.setLine(1, serverName);
            sign.update(true);
        }
    }

    public static void removeSign(Block block) {
        String serverName = getServerName(block.getLocation());

        if (serverName != null) {
            signs.remove(serverName);
            SSundeeHub.saveConf();
        }
    }

    public static boolean isRegisteredSign(Block block) {
        return getServerName(block.getLocation()) != null;
    }

    // Update the status of the given server
    public static void updateStatus(String serverName, int currentPlayers, int maxPlayers, String serverState) {
        BlockState state = signs.get(serverName).getLocation().getBlock().getState();

        if (state.getType().equals(Material.WALL_SIGN)) {
            Sign sign = (Sign) state;

            sign.setLine(2, String.format("%d/%d", currentPlayers, maxPlayers));
            sign.setLine(3, serverState);
            sign.update(true);
        }
    }

    public static boolean isServerFull(Block block) {
        BlockState state = block.getState();

        if (state.getType().equals(Material.WALL_SIGN)) {
            Sign sign = (Sign) state;
            String[] parts = sign.getLine(2).split("/");
            int currentPlayers;

            try {
                currentPlayers = Integer.parseInt(parts[0]);
            } catch (NumberFormatException e) {
                return true;
            }

            int maxPlayers;

            try {
                maxPlayers = Integer.parseInt(parts[1]);
            } catch (NumberFormatException e) {
                return true;
            }

            if (currentPlayers >= maxPlayers) {
                return true;
            }
        }

        return false;
    }

    public static String getServerStatus(Block block) {
        BlockState state = block.getState();
        String status = "restarting";

        if (state.getType().equals(Material.WALL_SIGN)) {
            Sign sign = (Sign) state;
            status = sign.getLine(3);
        }

        return status;
    }

    public static String getServerName(Block block) {
        return getServerName(block.getLocation());
    }

    public static String getServerName(Location signLocation) {
        for (Entry<String, ServerSign> entry : signs.entrySet()) {
            if (entry.getValue().getLocation().distance(signLocation) < 1d) {
                return entry.getKey();
            }
        }

        return null;
    }

    // Track players who have used the setsign command, but not yet clicked a sign
    public static void addPlayerEditing(String playerName, String[] args) {
        playersEditingSigns.put(playerName, args);
    }

    public static boolean isPlayerEditing(String playerName) {
        return playersEditingSigns.containsKey(playerName);
    }

    public static String[] endPlayerEditing(String playerName) {
        return playersEditingSigns.remove(playerName);
    }

    public static Set<String> getServers() {
        return signs.keySet();
    }
}
