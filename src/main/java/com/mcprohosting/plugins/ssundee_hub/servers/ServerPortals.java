package com.mcprohosting.plugins.ssundee_hub.servers;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.map.ServerPortal;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class ServerPortals {

    private static Map<String, ServerPortal> portals = null;
    private static ArrayList<String> playersEditingPortals = new ArrayList<>();

    // Load all previously created server signs
    public static void init() {
        portals = SSundeeHub.grabConfig().serverportals;
    }

    public static void addPortal(String destination, ServerPortal portal) {
        SSundeeHub.getPlugin().getLogger().info("Adding portal to config");
        portals.put(destination, portal);
        SSundeeHub.saveConf();
    }

    public static void removePortal(Block block) {
        ServerPortal portal = getServerPortalFromBlock(block);
        String key = null;

        if (portal != null) {
            key = getServer(portal);
        }

        if (key != null) {
            portals.remove(key);
            SSundeeHub.saveConf();
        }
    }

    public static String getServer(ServerPortal portal) {
        for (String server : portals.keySet()) {
            if (portals.get(server) == portal) {
                return server;
            }
        }

        return null;
    }

    public static ServerPortal getServerPortalFromBlock(Block block) {
        for (ServerPortal portal : portals.values()) {
            if (portal.containsBlock(block)) {
                return portal;
            }
        }

        return null;
    }

    public static boolean standingInBlock(Player player, int x, int y, int z) {
        Location location = new Location(player.getWorld(), x, y, z);
        if (player.getLocation().getBlock().equals(location.getBlock())) {
            return true;
        } else {
            return false;
        }
    }

    public static void addPlayerEditing(String player, String destination) {
        if (User.containsUser(player)) {
            playersEditingPortals.add(player);
            User.getUser(player).setEditingPortal(destination);
        }
    }

    public static boolean isPlayerEditing(String player) {
        return playersEditingPortals.contains(player);
    }

    public static void endPlayerEditing(String player) {
        playersEditingPortals.remove(player);
    }

    public static Set<String> getDestinations() {
        return portals.keySet();
    }

}
