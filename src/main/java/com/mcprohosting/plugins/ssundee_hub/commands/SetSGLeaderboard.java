package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.SGLeaderboard;
import com.mcprohosting.plugins.ssundee_hub.utilities.LocationUtil;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class SetSGLeaderboard implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("ssundeehub.editdynamiclbsigns") == false) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command!");
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command may only be used by a player.");
            return true;
        }

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("add")) {
                if (LocationUtil.isEditing(sender.getName()) == true) {
                    sender.sendMessage(ChatColor.RED + "You are already editing a leaderboard!");
                    return true;
                }

                LocationUtil.startEditing(sender.getName());
                sender.sendMessage(ChatColor.GREEN + "SG leaderboard sign editing enabled.");
                sender.sendMessage(ChatColor.GREEN + "Right click a sign to begin!");
                sender.sendMessage(ChatColor.GREEN + "After selecting two signs, type '/sglbsigns end' to enable the leaderboard.");
            } else if (args[0].equalsIgnoreCase("end")) {
                if (LocationUtil.isEditing(sender.getName()) == false) {
                    sender.sendMessage(ChatColor.RED + "You are not editing the SG leaderboard at this moment!");
                    return true;
                }

                ArrayList<Location> locations = LocationUtil.finishEditing(sender.getName());

                if (locations.size() != 2) {
                    sender.sendMessage(ChatColor.RED + "You must right click two signs! Try again!");
                    return true;
                }

                SGLeaderboard.getManager().createLeaderboard(locations);
                sender.sendMessage(ChatColor.GREEN + "SG leaderboard sign editing disabled.");
            } else if (args[0].equalsIgnoreCase("clear")) {
                LocationUtil.clearLocations(sender.getName());
                sender.sendMessage(ChatColor.GREEN + "SG leaderboard signs have been removed.");
            }

            return true;
        }

        return false;
    }
}
