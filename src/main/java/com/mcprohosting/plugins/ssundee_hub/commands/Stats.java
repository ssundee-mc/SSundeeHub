package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.Database;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.SGLeaderboardStat;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Stats implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("ssundeehub.statsother") == false && args.length == 1) {
            sender.sendMessage(ChatColor.RED + "You are not aloud to see other player's stats.");
            return true;
        }

        if (args.length > 1) {
            sender.sendMessage(ChatColor.RED + "Command Usage: /<command> [player]");
            return true;
        }

        String user;
        if (args.length == 1) {
            if (DatabaseManager.containsPlayer(args[0])) {
                user = args[0];
            } else {
                sender.sendMessage(ChatColor.RED + "That player does not exists in the database.");
                return true;
            }
        } else {
            if ((sender instanceof Player) == false) {
                sender.sendMessage(ChatColor.RED + "Specify a player name to see their stats");
                return true;
            }

            user = sender.getName();
        }

        SGLeaderboardStat stats = DatabaseManager.getPlayerLeadboardStats(User.getUser(user).getId());

        if (stats == null) {
            sender.sendMessage(ChatColor.RED + "Unable to retrieve "
                    + (args.length == 1 ? user + "'s" : "your") + " stats.");
            return true;
        }

        String[] message = new String[8];
        // Separator
        buildSeperator(message, 0);
        // Player Name
        message[1] = ChatColor.GREEN + (args.length == 1 ? user + "'s" : "Your") + " Stats:";
        // Separator
        buildSeperator(message, 2);
        // Tickets
        message[3] = ChatColor.GREEN + "Tickets: " + ChatColor.GOLD + User.getUser(user).getTickets();
        // Points
        message[4] = ChatColor.GREEN + "Points: " + ChatColor.GOLD + stats.getPoints();
        // Wins/Losses
        message[5] = ChatColor.GREEN + "Total Games: " + ChatColor.GOLD + stats.getWins() + "W" +
                ChatColor.GRAY + "/" + ChatColor.RED + (stats.getGames() - stats.getWins()) + "L";
        // Kills
        message[6] = ChatColor.GREEN + "Kills: " + ChatColor.GOLD + stats.getKills();
        // Deaths
        message[7] = ChatColor.GREEN + "Deaths: " + ChatColor.RED + stats.getDeaths();

        sender.sendMessage(message);
        return true;
    }

    public void buildSeperator(String[] message, int index) {
        message[index] = ChatColor.GOLD.toString();
        for (int x = 0; x < 21; x++) {
            message[index] += "=";
        }
    }
}
