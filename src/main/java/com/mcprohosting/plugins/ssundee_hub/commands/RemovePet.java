package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.Pet;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemovePet implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }

        Player sender = (Player) commandSender;
        if (!sender.hasPermission("ssundeehub.addremoveitems")) {
            return false;
        }

        if (strings.length < 1) {
            sender.sendMessage(ChatColor.RED + "Usage: /removepet <id>");
            sender.sendMessage(ChatColor.RED + "Possible pet IDs:");
            for (Pet p : Pet.getPets()) {
                sender.sendMessage(p.id + ") " + p.type.toString());
            }
        } else {
            Pet toBeDeleted = null;
            for (Pet p : Pet.getPets()) {
                if (p.id == Integer.parseInt(strings[0])) {
                    toBeDeleted = p;
                }
            }
            if (toBeDeleted == null) {
                sender.sendMessage(ChatColor.RED + "Unable to find that pet!");
                return true;
            }
            DatabaseManager.removePetFromDB(toBeDeleted);
            sender.sendMessage(ChatColor.GREEN + "Removed pet from database.");
            Pet.refreshPets();
        }

        return true;
    }
}
