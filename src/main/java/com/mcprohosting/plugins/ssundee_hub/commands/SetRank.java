package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SetRank implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 2) {
            return false;
        }

        if (sender.hasPermission("ssundeehub.setrank")) {
            String name = args[0];
            String rank = args[1];

            if (!DatabaseManager.containsPlayer(name)) {
                DatabaseManager.addPlayer(name);
            }

            boolean updated = false;
            for (String value : SSundeeHub.getPerms().getGroups()) {
                if (value.toString().toLowerCase().equals(rank.toLowerCase())) {
                    DatabaseManager.updateRank(name, value);
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "perm player " + name + " setgroup " + rank);

                    if (User.containsUser(name)) {
                        User.getUser(name).setRank(value);
                    }

                    updated = true;
                }
            }

            if (updated == false) {
                SSundeeHub.getPlugin().getLogger().info(name + "'s rank was not updated. Something went wrong!");
                return true;
            }

            sender.sendMessage(ChatColor.GREEN + name + "'s rank has been updated to " + rank + ".");
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
        }

        return true;
    }
}
