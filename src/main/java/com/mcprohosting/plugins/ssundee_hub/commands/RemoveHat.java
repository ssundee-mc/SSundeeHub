package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemoveHat implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }

        Player sender = (Player) commandSender;
        if (!sender.hasPermission("ssundeehub.addremoveitems")) {
            return false;
        }

        if (strings.length < 1) {
            sender.sendMessage(ChatColor.RED + "Usage: /removehat <id>");
            sender.sendMessage(ChatColor.RED + "Possible hat IDs:");
            for (HatItem h : HatItem.getServerHats()) {
                sender.sendMessage(h.id + ") " + h.playerName);
            }
        } else {
            HatItem hatToRemove = null;
            for (HatItem h : HatItem.getServerHats()) {
                if (h.id == Integer.parseInt(strings[0])) {
                    hatToRemove = h;
                }
            }
            if (hatToRemove == null) {
                sender.sendMessage(ChatColor.RED + "Unable to find that hat!");
                return true;
            }
            DatabaseManager.removeHatFromDB(hatToRemove);
            sender.sendMessage(ChatColor.GREEN + "Removed hat from database.");
            HatItem.refreshHats();
        }

        return true;
    }
}
