package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.servers.ServerPortals;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetPortal implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("ssundeehub.editserverportals")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command may only be used by a player.");
            return true;
        }

        if (args.length == 1) {
            ServerPortals.addPlayerEditing(sender.getName(), args[0]);
            sender.sendMessage(ChatColor.DARK_GREEN + "Right click two blocks to set a portal to " + args[0]);
            return true;
        }

        return false;
    }

}
