package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.servers.ServerSigns;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSign implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("ssundeehub.editserversigns")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage("This command may only be used by a player.");
            return true;
        }

        if (args.length == 2) {
            ServerSigns.addPlayerEditing(sender.getName(), args);
            sender.sendMessage(ChatColor.GREEN + "Right click a sign to set it as the server sign for " + args[1]);
            return true;
        }

        return false;
    }
}
