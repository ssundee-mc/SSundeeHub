package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NewSGItem implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        Player sender = (Player) commandSender;

        if (!sender.hasPermission("ssundeehub.addremoveitems")) {
            return false;
        }

        if (strings.length < 1) {
            sender.sendMessage(ChatColor.RED + "Usage: /addsgitem <Material Number> <Price> <Effect> <Name> <Description>");
            sender.sendMessage(ChatColor.RED + "Valid effect values: NONE NO_SLOWNESS NO_POISON NO_LIGHTNING ONESHOT_SKELETONS ONESHOT_ZOMBIES NO_BLINDNESS NO_ACIDRAIN NO_WEAKNESS");
            return true;
        }

        if (strings.length >= 5) {
            String material = strings[0];
            String price = strings[1];
            String effect = strings[2];
            String name = strings[3];
            String description = strings[4];

            for (int x = 5; x < strings.length; x++) {
                description += " " + strings[x];
            }

            DatabaseManager.addItemToShopDB(Integer.parseInt(material), "sg", name, description,
                    effect, Integer.parseInt(price));
            sender.sendMessage(ChatColor.GREEN + "Added item " + name + " to the DB!");
        }
        return true;
    }

}
