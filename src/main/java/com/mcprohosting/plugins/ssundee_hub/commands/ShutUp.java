package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.entities.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShutUp implements CommandExecutor {

    private static boolean muted = false;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender.hasPermission("ssundeehub.shutup") == false) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        muted = !muted;

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (muted) {
                player.sendMessage(ChatColor.RED + "Chat has been temporarily disabled to prevent spam.");
            } else {
                player.sendMessage(ChatColor.GREEN + "Chat has been enabled once again!");
            }
        }

        for (User u : User.getUsers()) {
            switch (u.getRank().toString().toLowerCase()) {
                case "default":
                    u.setMuted(muted);
                    continue;
                case "lapis":
                    u.setMuted(muted);
                    continue;
                case "redstone":
                    u.setMuted(muted);
                    continue;
                case "diamond":
                    u.setMuted(muted);
                    continue;
                case "youtuber":
                    u.setMuted(false);
                    continue;
                case "mod":
                    u.setMuted(false);
                    continue;
                case "admin":
                    u.setMuted(false);
                    continue;
                case "dev":
                    u.setMuted(false);
                    continue;
                case "owner":
                    u.setMuted(false);
                    continue;
                default:
                    u.setMuted(muted);
                    continue;
            }
        }

        return true;
    }

    public static boolean getMuted() {
        return muted;
    }
}
