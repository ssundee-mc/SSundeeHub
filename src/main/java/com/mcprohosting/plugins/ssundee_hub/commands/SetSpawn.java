package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("ssundeehub.setspawn")) {
                SSundeeHub.grabConfig().setSpawn(player.getLocation());
                player.sendMessage(ChatColor.GREEN + "You set the server spawn to your location.");
            } else {
                player.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
            }
        } else {
            sender.sendMessage(ChatColor.RED + "This command may only be used by a player.");
        }

        return true;
    }
}
