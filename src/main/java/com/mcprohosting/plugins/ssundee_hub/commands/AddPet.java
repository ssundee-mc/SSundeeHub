package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.Pet;
import com.mcprohosting.plugins.ssundee_hub.entities.PetType;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddPet implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }

        Player sender = (Player) commandSender;
        if (!sender.hasPermission("ssundeehub.addremoveitems")) {
            return false;
        }

        if (strings.length < 2) {
            sender.sendMessage(ChatColor.RED + "Usage: /addpet <Pet Type> <price>");
            sender.sendMessage(ChatColor.RED + "Possible pet types: wolf ocelot");
        } else {
            PetType t = PetType.getPetTypeForName(strings[0]);
            if (t == PetType.INVALID) {
                sender.sendMessage(ChatColor.RED + "Invalid Pet type!");
                sender.sendMessage(ChatColor.RED + "Possible pet types: wolf ocelot");
                return true;
            }
            DatabaseManager.addPetToDB(t, Integer.parseInt(strings[1]));
            Pet.refreshPets();
            sender.sendMessage(ChatColor.GREEN + "Added pet to DB.");

        }

        return true;
    }
}
