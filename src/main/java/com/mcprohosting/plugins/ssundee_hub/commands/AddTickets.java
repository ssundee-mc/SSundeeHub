package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AddTickets implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 2) {
            return false;
        }

        if (sender.hasPermission("ssundeehub.addtickets")) {
            String name = args[0];
            int tickets = Integer.parseInt(args[1]);

            if (!DatabaseManager.containsPlayer(name)) {
                DatabaseManager.addPlayer(name);
            }

            if (DatabaseManager.addTickets(name, tickets) == false) {
                SSundeeHub.getPlugin().getLogger().info(name + "'s tickets were not updated. Something went wrong!");
            }

            sender.sendMessage(ChatColor.GREEN.toString() + tickets + " have been added to " + name + "'s account.");
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
        }

        return true;
    }
}
