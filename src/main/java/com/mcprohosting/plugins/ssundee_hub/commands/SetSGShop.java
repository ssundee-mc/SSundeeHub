package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.utilities.UtilityMethods;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

public class SetSGShop implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        Player sender = (Player) commandSender;
        if (!sender.hasPermission("ssundeehub.setsgshop")) { // Temp restriction, permissions later.
            return false;
        }
        Block playerLookingAt = sender.getTargetBlock(null, 10); // 10 Blocks should be enough TODO Avoid deprecated method.
        if (!(playerLookingAt.getType() == Material.CHEST)) {
            sender.sendMessage(ChatColor.RED + "Please look at a chest!");
            return true;
        }

        Location chestLocation = playerLookingAt.getLocation();
        if (SSundeeHub.grabConfig().sgshopLocation == null) {
            SSundeeHub.grabConfig().sgshopLocation = UtilityMethods.locToString(chestLocation);
            UtilityMethods.setChestName(chestLocation, "Survival Games Shop");
            sender.sendMessage(ChatColor.GREEN + "Set this chest to be the Survival Games Item Shop.");
        } else {
            SSundeeHub.grabConfig().sgshopLocation = null;
            sender.sendMessage(ChatColor.GREEN + "This chest is no longer the Survival Games Item Shop and has been removed.");
            chestLocation.getWorld().getBlockAt(chestLocation).setType(Material.AIR);
        }

        SSundeeHub.getPlugin().saveConfig();
        return true;
    }
}
