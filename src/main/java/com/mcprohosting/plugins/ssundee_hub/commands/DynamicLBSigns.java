package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic.DynamicLeaderboardSigns;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DynamicLBSigns implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("ssundeehub.editdynamiclbsigns")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage("This command may only be used by a player.");
            return true;
        }

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("add")) {
                DynamicLeaderboardSigns.addPlayerEditing(sender.getName());
                sender.sendMessage(ChatColor.GREEN + "Leaderboard sign editing enabled.");
                sender.sendMessage(ChatColor.GREEN + "Right click to signs in the order in which you would like the leaderboard to appear.");
                sender.sendMessage(ChatColor.GREEN + "When finished, type '/lbsigns end' to stop adding signs.");
            } else if (args[0].equalsIgnoreCase("end")) {
                if (DynamicLeaderboardSigns.isPlayerEditing(sender.getName())) {
                    DynamicLeaderboardSigns.removePlayerEditing(sender.getName());
                    sender.sendMessage(ChatColor.GREEN + "Leaderboard sign editing disabled.");
                } else {
                    sender.sendMessage(ChatColor.GREEN + "Leaderboard sign editing is already disabled.");
                }
            } else if (args[0].equalsIgnoreCase("clear")) {
                DynamicLeaderboardSigns.clearAll();
                sender.sendMessage(ChatColor.GREEN + "All leaderboard signs have been removed.");
            }

            return true;
        }

        return false;
    }
}
