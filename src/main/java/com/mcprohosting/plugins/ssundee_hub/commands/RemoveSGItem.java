package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.items.MinigameShopItem;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus.SurvivalGamesShop;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemoveSGItem implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        Player sender = (Player) commandSender;

        if (!sender.hasPermission("ssundeehub.addremoveitems")) {
            return false;
        }

        if (strings.length < 1) {
            sender.sendMessage(ChatColor.RED + "Usage: /removesgitem <id>");
            sender.sendMessage(ChatColor.RED + "ID List:");
            for (MinigameShopItem i : DatabaseManager.getMinigameShopItemsFromDatabase("sg")) {
                sender.sendMessage(ChatColor.RED + Integer.toString(i.id) + ") " + i.name);
            }
        } else {
            MinigameShopItem itemToRemove = null;
            for (MinigameShopItem i : DatabaseManager.getMinigameShopItemsFromDatabase("sg")) {
                if (i.id == Integer.parseInt(strings[0])) {
                    itemToRemove = i;
                }
            }
            if (itemToRemove == null) {
                sender.sendMessage(ChatColor.RED + "Unable to find that item!");
                return true;
            }
            DatabaseManager.removeItemFromDB(itemToRemove);
            sender.sendMessage(ChatColor.GREEN + "Removed item with id " + itemToRemove.id);
        }

        return true;
    }
}
