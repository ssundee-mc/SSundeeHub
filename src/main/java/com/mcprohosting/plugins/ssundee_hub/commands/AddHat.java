package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddHat implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }

        Player sender = (Player) commandSender;
        if (!sender.hasPermission("ssundeehub.addremoveitems")) {
            return false;
        }

        if (strings.length == 0) {
            sender.sendMessage(ChatColor.RED + "Usage: /addhat head <Head Username> <price>");
            sender.sendMessage(ChatColor.RED + "Usage: /addhat item <item id> <item data> <price>");
            return true;
        }

        if (strings[0].equalsIgnoreCase("item")) {
            if (strings.length < 4) {
                sender.sendMessage(ChatColor.RED + "Usage: /addhat item <item id> <item data> <price>");
            } else {
                sender.sendMessage(ChatColor.GREEN + "Added hat to DB.");
                try {
                    DatabaseManager.addHatToDB(Integer.parseInt(strings[3]),
                            Integer.parseInt(strings[1]),
                            Integer.parseInt(strings[2]));
                } catch (NumberFormatException e) {
                    sender.sendMessage(ChatColor.RED + "Item id, item data, and price must be a valid number!");
                }
                HatItem.refreshHats();
            }
        }

        if (strings[0].equalsIgnoreCase("head")) {
            if (strings.length < 3) {
                sender.sendMessage(ChatColor.RED + "Usage: /addhat head <Head Username> <price>");
            } else {
                sender.sendMessage(ChatColor.GREEN + "Added hat to DB.");
                DatabaseManager.addHatToDB(Integer.parseInt(strings[2]), strings[1]);
                HatItem.refreshHats();
            }
        }

        return true;
    }
}
