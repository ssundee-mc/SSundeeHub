package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Version implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arg) {
        sender.sendMessage(ChatColor.GREEN + "SsundeeHub Version: " + SSundeeHub.getPlugin().getDescription().getVersion());
        return true;
    }
}
