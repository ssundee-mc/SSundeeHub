package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.Pet;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetPetName implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        Player p = (Player) commandSender;

        if (strings.length < 1) {
            p.sendMessage(ChatColor.RED + "Invalid usage!");
            p.sendMessage(ChatColor.RED + "Usage:/setpetname <Desired Name>");
            p.sendMessage(ChatColor.RED.toString() + ChatColor.ITALIC.toString() + "NOTE: This will only affect your active pet!");
            return true;
        }

        Pet activePet = DatabaseManager.getActivePetForPlayer(p);
        if (activePet == null) {
            p.sendMessage(ChatColor.RED + "You must have a pet out to rename it!");
            return true;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(strings[0]);
        for (int i = 1; i < strings.length; i++) {
            sb.append(" ").append(strings[i]);
        }
        DatabaseManager.updatePetNameFor(User.getUser(p.getName()).getId(), activePet.id, sb.toString());
        User u = User.getUser(p.getName());
        if (u.currentPet != null) {
            u.currentPet.remove();
        }
        u.currentPet = DatabaseManager.getActivePetForPlayer(p).spawnPet();
        return true;

    }
}
