package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.items.MinigameShopItem;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class MinigameItems implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        Player p = (Player) commandSender;
        ArrayList<MinigameShopItem> sgItems = DatabaseManager.getUserItemsFromDatabase(User.getUser(p.getName()).getId(), "sg");
        if (sgItems.size() > 0) {
            p.sendMessage(ChatColor.GREEN + "You own the following items for the Survival Games:");
            int counter = 1;
            for (MinigameShopItem i : sgItems) {
                p.sendMessage(ChatColor.GREEN + Integer.toString(counter) + ") " + i.name + " - " + i.description);
                counter++;
            }
        } else {
            p.sendMessage(ChatColor.RED + "You have not purchased any items. :(");
        }


        return true;
    }
}
