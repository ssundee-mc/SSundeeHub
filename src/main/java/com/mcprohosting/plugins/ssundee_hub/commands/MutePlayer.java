package com.mcprohosting.plugins.ssundee_hub.commands;

import com.mcprohosting.plugins.ssundee_hub.entities.User;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

public class MutePlayer implements CommandExecutor {
    private static ArrayList<String> players = new ArrayList<String>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (sender.hasPermission("ssundeehub.muteplayer") == false) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
            return true;
        }

        if (strings.length < 1 || strings.length > 2) {
            sender.sendMessage(ChatColor.RED + "/<command> <player> [time in minutes]");
            return true;
        }

        User user;
        if (User.containsUser(strings[0]) == false) {
            sender.sendMessage(ChatColor.RED + "That user is not online.");
            return true;
        }
        user = User.getUser(strings[0]);

        int minutes = 30;
        if (strings.length == 2) {
            try {
                minutes = Integer.parseInt(strings[1]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.RED + "You must specify a valid number for the time argument.");
                return true;
            }
        }

        user.setMuteTime(minutes);

        return true;
    }

    public static ArrayList<String> getPlayers() {
        return players;
    }
}
