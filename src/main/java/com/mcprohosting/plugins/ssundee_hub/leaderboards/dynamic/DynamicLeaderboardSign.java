package com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic;

import com.mcprohosting.plugins.ssundee_hub.config.ConfigObject;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.SGLeaderboardStat;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

@Deprecated
public class DynamicLeaderboardSign extends ConfigObject {

    private SGLeaderboardStat data = null;

    public DynamicLeaderboardSign() {
    }

    public DynamicLeaderboardSign(Location location) {
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
    }

    public int x = 0;
    public int y = 0;
    public int z = 0;

    public Location getLocation() {
        return new Location(Bukkit.getWorlds().get(0), x, y, z);
    }

    public void updateData(SGLeaderboardStat data) {
        this.data = data;
    }

    public void updateSign() {
        if (data == null) {
            return;
        }

        Block block = getLocation().getBlock();
        switch (UpdateDynamicLeaderboardSigns.getInfoIndex()) {
            case 0:
                if (block.getType().equals(Material.WALL_SIGN)) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(1, data.getPlayer());
                    sign.setLine(2, "Points:");
                    sign.setLine(3, String.format("%d", data.getPoints()));
                    sign.update(true);
                }
                break;
            case 1:
                if (block.getType().equals(Material.WALL_SIGN)) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(1, data.getPlayer());
                    sign.setLine(2, "Games Played:");
                    sign.setLine(3, String.format("%d", data.getGames()));
                    sign.update(true);
                }
                break;
            case 2:
                if (block.getType().equals(Material.WALL_SIGN)) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(1, data.getPlayer());
                    sign.setLine(2, "Wins:");
                    sign.setLine(3, String.format("%d", data.getWins()));
                    sign.update(true);
                }
                break;
            case 3:
                if (block.getType().equals(Material.WALL_SIGN)) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(1, data.getPlayer());
                    sign.setLine(2, "Losses:");
                    sign.setLine(3, String.format("%d", data.getGames() - data.getWins()));
                    sign.update(true);
                }
                break;
            case 4:
                if (block.getType().equals(Material.WALL_SIGN)) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(1, data.getPlayer());
                    sign.setLine(2, "Kills:");
                    sign.setLine(3, String.format("%d", data.getKills()));
                    sign.update(true);
                }
                break;
            case 5:
                if (block.getType().equals(Material.WALL_SIGN)) {
                    Sign sign = (Sign) block.getState();
                    sign.setLine(1, data.getPlayer());
                    sign.setLine(2, "Deaths:");
                    sign.setLine(3, String.format("%d", data.getDeaths()));
                    sign.update(true);
                }
                break;
            default:
                break;
        }
    }
}
