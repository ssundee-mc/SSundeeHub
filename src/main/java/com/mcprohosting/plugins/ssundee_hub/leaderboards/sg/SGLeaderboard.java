package com.mcprohosting.plugins.ssundee_hub.leaderboards.sg;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import java.util.ArrayList;

public class SGLeaderboard {

    private static SGLeaderboard manager;
    private Leaderboard leaderboard;
    private ArrayList<SGLeaderboardStat> stats;

    public SGLeaderboard() {
        manager = this;
        leaderboard = SSundeeHub.grabConfig().sgleaderboard;
        stats = DatabaseManager.getTopEight();

        if (isValid() == false) {
            leaderboard = null;
            SSundeeHub.getPlugin().getLogger().info("Leaderboard is not valid!");
        }
    }

    public boolean leaderboardExist() {
        if (leaderboard == null) {
            return false;
        }

        return !((leaderboard.getLocationOne() == null) && (leaderboard.getLocationTwo() == null));
    }

    public void createLeaderboard(ArrayList<Location> locations) {
        leaderboard = new Leaderboard(locations.get(0), locations.get(1));
        SSundeeHub.grabConfig().sgleaderboard = leaderboard;

        if (isValid() == false) {
            leaderboard = null;
            SSundeeHub.getPlugin().getLogger().info("Leaderboard is not valid!");
        }
    }

    public static SGLeaderboard getManager() {
        return manager;
    }

    private boolean isValid() {
        if (leaderboardExist()) {
            if (Math.abs(leaderboard.getLocationOne().getBlockX() - leaderboard.getLocationTwo().getBlockX()) == 7) {
                if (Math.abs(leaderboard.getLocationOne().getBlockY() - leaderboard.getLocationTwo().getBlockY()) == 1 &&
                        leaderboard.getLocationOne().getBlockZ() == leaderboard.getLocationTwo().getBlockZ()) {
                    return true;
                }
            }
            if (Math.abs(leaderboard.getLocationOne().getBlockZ() - leaderboard.getLocationTwo().getBlockZ()) == 7) {
                if (Math.abs(leaderboard.getLocationOne().getBlockY() - leaderboard.getLocationTwo().getBlockY()) == 1 &&
                        leaderboard.getLocationOne().getBlockX() == leaderboard.getLocationTwo().getBlockX()) {
                    return true;
                }
            }
        }

        return false;
    }

    public void updateLeaderboard() {
        if (isValid() == false) {
            return;
        }

        int xVal = leaderboard.getLocationOne().getBlockX() - leaderboard.getLocationTwo().getBlockX();
        int zVal = leaderboard.getLocationOne().getBlockZ() - leaderboard.getLocationTwo().getBlockZ();
        String outer = "";

        if (Math.abs(xVal) == 7) {
            int index = 0;
            for (int y = 0; y < 2; y++) {
                for (int offset = 0; offset < 4; offset++, index++) {
                    if (index > stats.size() - 1) {
                        break;
                    }

                    for (int x = 0; x < 8; x++) {
                        Block block = null;
                        if (xVal == -7) {
                            block = new Location(Bukkit.getWorlds().get(0),
                                    leaderboard.getLocationOne().getBlockX() + x,
                                    leaderboard.getTopY() - y,
                                    leaderboard.getLocationOne().getZ()).getBlock();
                        } else if (xVal == 7) {
                            block = new Location(Bukkit.getWorlds().get(0),
                                    leaderboard.getLocationOne().getBlockX() - x,
                                    leaderboard.getTopY() - y,
                                    leaderboard.getLocationOne().getZ()).getBlock();
                        }

                        if (block == null) {
                            continue;
                        }

                        if (block.getType().equals(Material.WALL_SIGN)) {
                            Sign sign = (Sign) block.getState();
                            switch (x) {
                                case 0:
                                    sign.setLine(offset, (index + 1) + ".");
                                    break;
                                case 1:
                                    sign.setLine(offset, stats.get(index).getPlayer());
                                    break;
                                case 2:
                                    sign.setLine(offset, stats.get(index).getPoints() + "");
                                    break;
                                case 3:
                                    sign.setLine(offset, stats.get(index).getGames() + "");
                                    break;
                                case 4:
                                    sign.setLine(offset, stats.get(index).getWins() + "");
                                    break;
                                case 5:
                                    sign.setLine(offset, stats.get(index).getGames() - stats.get(index).getWins() + "");
                                    break;
                                case 6:
                                    sign.setLine(offset, stats.get(index).getKills() + "");
                                    break;
                                case 7:
                                    sign.setLine(offset, stats.get(index).getDeaths() + "");
                                    break;
                            }
                            sign.update(true);
                        }
                    }
                }
            }
        }
        if (Math.abs(zVal) == 7) {
            int index = 0;
            for (int y = 0; y < 2; y++) {
                for (int offset = 0; offset < 4; offset++, index++) {
                    if (index > stats.size() - 1) {
                        break;
                    }

                    for (int x = 0; x < 8; x++) {
                        Block block = null;
                        if (zVal == -7) {
                            block = new Location(Bukkit.getWorlds().get(0),
                                    leaderboard.getLocationOne().getBlockX(),
                                    leaderboard.getTopY() - y,
                                    leaderboard.getLocationOne().getZ() + x).getBlock();
                        } else if (zVal == 7) {
                            block = new Location(Bukkit.getWorlds().get(0),
                                    leaderboard.getLocationOne().getBlockX(),
                                    leaderboard.getTopY() - y,
                                    leaderboard.getLocationOne().getZ() - x).getBlock();
                        }

                        if (block == null) {
                            continue;
                        }

                        if (block.getType().equals(Material.WALL_SIGN)) {
                            Sign sign = (Sign) block.getState();
                            switch (x) {
                                case 0:
                                    sign.setLine(offset, (index + 1) + ".");
                                    break;
                                case 1:
                                    sign.setLine(offset, stats.get(index).getPlayer());
                                    break;
                                case 2:
                                    sign.setLine(offset, stats.get(index).getPoints() + "");
                                    break;
                                case 3:
                                    sign.setLine(offset, stats.get(index).getGames() + "");
                                    break;
                                case 4:
                                    sign.setLine(offset, stats.get(index).getWins() + "");
                                    break;
                                case 5:
                                    sign.setLine(offset, stats.get(index).getGames() - stats.get(index).getWins() + "");
                                    break;
                                case 6:
                                    sign.setLine(offset, stats.get(index).getKills() + "");
                                    break;
                                case 7:
                                    sign.setLine(offset, stats.get(index).getDeaths() + "");
                                    break;
                                default:
                                    break;
                            }
                            sign.update(true);
                        }
                    }
                }
            }
        }
    }

    public void updateStats() {
        stats = DatabaseManager.getTopEight();
        updateLeaderboard();
    }

}
