package com.mcprohosting.plugins.ssundee_hub.leaderboards.sg;

import com.mcprohosting.plugins.ssundee_hub.config.ConfigObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Leaderboard extends ConfigObject {

    public Leaderboard() {
    }

    public Leaderboard(Location cornerOne, Location cornerTwo) {
        x1 = cornerOne.getBlockX();
        x2 = cornerTwo.getBlockX();
        y1 = cornerOne.getBlockY();
        y2 = cornerTwo.getBlockY();
        z1 = cornerOne.getBlockZ();
        z2 = cornerTwo.getBlockZ();
    }

    public int x1;
    public int y1;
    public int z1;
    public int x2;
    public int y2;
    public int z2;

    public Location getLocationOne() {
        return new Location(Bukkit.getWorlds().get(0), x1, y1, z1);
    }

    public Location getLocationTwo() {
        return new Location(Bukkit.getWorlds().get(0), x2, y2, z2);
    }

    public int getTopY() {
        return (getLocationOne().getBlockY() > getLocationTwo().getBlockY() ? getLocationOne().getBlockY() : getLocationTwo().getBlockY());
    }
}
