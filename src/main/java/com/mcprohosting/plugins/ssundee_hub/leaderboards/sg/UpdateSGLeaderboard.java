package com.mcprohosting.plugins.ssundee_hub.leaderboards.sg;

public class UpdateSGLeaderboard implements Runnable {

    @Override
    public void run() {
        SGLeaderboard.getManager().updateStats();
    }

}
