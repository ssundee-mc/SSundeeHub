package com.mcprohosting.plugins.ssundee_hub.leaderboards.sg;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SGLeaderboardStat {

    String name;
    int points;
    int games;
    int wins;
    int kills;
    int deaths;

    public SGLeaderboardStat(ResultSet rs) {
        try {
            name = rs.getString("name");
            points = rs.getInt("totalPoints");
            games = rs.getInt("totalGames");
            wins = rs.getInt("wins");
            kills = rs.getInt("totalKills");
            deaths = rs.getInt("totalDeaths");
        } catch (SQLException e) {
            // Error!
        }
    }

    public String getPlayer() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public int getGames() {
        return games;
    }

    public int getWins() {
        return wins;
    }

    public int getKills() {
        return kills;
    }

    public int getDeaths() {
        return deaths;
    }

}
