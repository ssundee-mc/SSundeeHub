package com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.leaderboards.sg.SGLeaderboardStat;

import java.util.ArrayList;

@Deprecated
public class UpdateDynamicLeaderboardData implements Runnable {

    @Override
    public void run() {
        ArrayList<SGLeaderboardStat> data = DatabaseManager.getTopEight();
        ArrayList<String> players = new ArrayList<String>();

        int index = 0;
        for (int x = 0; x < data.size(); x++) {
            if (x > DynamicLeaderboardSigns.getLeaderboardSigns().size() - 1) {
                break;
            }

            DynamicLeaderboardSigns.getLeaderboardSigns().get(x).updateData(data.get(x));
        }
    }

}
