package com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic;

@Deprecated
public class UpdateDynamicLeaderboardSigns implements Runnable {

    private static int infoIndex = 0;

    @Override
    public void run() {
        for (DynamicLeaderboardSign sign : DynamicLeaderboardSigns.getLeaderboardSigns()) {
            sign.updateSign();
        }

        infoIndex++;

        if (infoIndex == 6) {
            infoIndex = 0;
        }
    }

    public static int getInfoIndex() {
        return infoIndex;
    }

}
