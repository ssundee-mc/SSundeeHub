package com.mcprohosting.plugins.ssundee_hub.leaderboards.dynamic;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import java.util.ArrayList;

@Deprecated
public class DynamicLeaderboardSigns {
    private static ArrayList<DynamicLeaderboardSign> signs = null;
    private static ArrayList<String> playersEditingSigns = new ArrayList<>();

    public static void init() {
        signs = SSundeeHub.grabConfig().leaderboardsigns;
    }

    public static void addSign(Block block) {
        signs.add(new DynamicLeaderboardSign(block.getLocation()));
        SSundeeHub.saveConf();

        if (block.getType().equals(Material.WALL_SIGN)) {
            Sign sign = (Sign) block.getState();
            sign.setLine(0, String.format("[%d]", signs.size()));
            sign.update(true);
        }
    }

    public static void removeSign(Block block) {
        int index = indexOf(block);

        if (index >= 0) {
            signs.remove(index);
            SSundeeHub.saveConf();
        }
    }

    public static boolean isRegisteredSign(Block block) {
        return (indexOf(block) >= 0);
    }

    private static int indexOf(Block block) {
        for (int i = 0; i < signs.size(); i++) {
            if (signs.get(i).getLocation().distance(block.getLocation()) < 1) {
                return i;
            }
        }

        return -1;
    }

    // Update the text on the given leaderboard sign
    public static DynamicLeaderboardSign getSign(int rank) {
        return signs.get(rank);
    }

    public static ArrayList<DynamicLeaderboardSign> getLeaderboardSigns() {
        return signs;
    }

    public static void clearAll() {
        for (DynamicLeaderboardSign s : signs) {
            Block block = s.getLocation().getBlock();
            if (block.getType().equals(Material.WALL_SIGN)) {
                Sign sign = (Sign) block.getState();
                sign.setLine(0, "");
                sign.setLine(1, "");
                sign.setLine(2, "");
                sign.setLine(3, "");
                sign.update(true);
            }
        }

        signs.clear();
    }

    public static void addPlayerEditing(String playerName) {
        playersEditingSigns.add(playerName);
    }

    public static boolean isPlayerEditing(String playerName) {
        return playersEditingSigns.contains(playerName);
    }

    public static void removePlayerEditing(String playerName) {
        playersEditingSigns.remove(playerName);
    }
}
