package com.mcprohosting.plugins.ssundee_hub.utilities;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import net.minecraft.server.v1_7_R1.TileEntityChest;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_7_R1.block.CraftChest;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

public class UtilityMethods {
    public static boolean canChangeBlocks(Player player) {
        return player.hasPermission("ssundeehub.changeblocks");
    }

    public static boolean canDropItems(Player player) {
        return player.hasPermission("ssundeehub.dropitems");
    }

    public static void redirectPlayerToServer(Player player, String serverName) {
        if (System.currentTimeMillis() - User.getUser(player.getName()).getLastTraveling() >= 5000) {
            try {
                ByteArrayOutputStream b = new ByteArrayOutputStream();
                DataOutputStream out = new DataOutputStream(b);

                out.writeUTF("Connect");
                out.writeUTF(serverName);

                player.sendPluginMessage(SSundeeHub.getPlugin(), "BungeeCord", b.toByteArray());
            } catch (IOException e) {
                SSundeeHub.getPlugin().getLogger().severe(String.format("Error teleporting %s to server: %s", player.getName(), serverName));
            }

            User.getUser(player.getName()).setLastTraveling(System.currentTimeMillis());
        }
    }

    public static String locToString(Location l) {
        if (l.getPitch() == 0 && l.getYaw() == 0) {
            return l.getWorld().getName() + "@" + l.getX() + "@" + l.getY() + "@" + l.getZ();
        } else {
            return l.getWorld().getName() + "@" + l.getX() + "@" + l.getY() + "@" + l.getZ() + "@" + l.getYaw() + "@" + l.getPitch();
        }
    }

    public static Location stringToLoc(String s) {
        String[] arr = s.split("@");

        if (arr.length == 4) {
            return new Location(Bukkit.getWorld(arr[0]), Double.parseDouble(arr[1]), Double.parseDouble(arr[2]), Double.parseDouble(arr[3]));
        } else {
            return new Location(Bukkit.getWorld(arr[0]), Double.parseDouble(arr[1]), Double.parseDouble(arr[2]), Double.parseDouble(arr[3]),
                    Float.parseFloat(arr[4]), Float.parseFloat(arr[5]));
        }
    }

    public static Chest getSGShopChest() {
        Chest sgChest = null;
        String locationString = SSundeeHub.grabConfig().sgshopLocation;
        if (locationString == null) {
            return null;
        }
        Location locOfChest = stringToLoc(locationString);
        Block chestMaybe = locOfChest.getBlock();
        if (chestMaybe == null) {
            return null;
        }
        if (chestMaybe.getType() != Material.CHEST) {
            return null;
        }
        sgChest = (Chest) chestMaybe.getState();
        return sgChest;
    }

    public static void setChestName(Location loc, String name) {
        try {
            loc.getBlock().setType(Material.CHEST);

            Field inventoryField = CraftChest.class.getDeclaredField("chest");
            inventoryField.setAccessible(true);
            TileEntityChest teChest = ((TileEntityChest) inventoryField.get((CraftChest) loc.getBlock().getState()));
            teChest.a(name);
        } catch (NoSuchFieldException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to set chest name!");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            SSundeeHub.getPlugin().getLogger().info("Unable to set chest name!");
            e.printStackTrace();
        }
    }

    public static Location calculateTeleportLocation(Player player) {
        User user = User.getUser(player.getName());
        int distance = 5 * user.getTravelPowerLevel(TravelPower.TELEPORT);
        Block target = player.getTargetBlock(null, distance);
        Block feetPos;

        if (target != null && !target.isEmpty()) {
            feetPos = target.getRelative(BlockFace.UP);
            while (!feetPos.isEmpty() && !feetPos.getRelative(BlockFace.UP).isEmpty()) {
                feetPos = feetPos.getRelative(BlockFace.UP);
            }
            Location loc = feetPos.getLocation();
            loc.setYaw(player.getLocation().getYaw());
            loc.setPitch(player.getLocation().getPitch());
            loc.setX(loc.getX() + 0.5);
            loc.setZ(loc.getZ() + 0.5);
            return loc;
        }

        return null;
    }
}
