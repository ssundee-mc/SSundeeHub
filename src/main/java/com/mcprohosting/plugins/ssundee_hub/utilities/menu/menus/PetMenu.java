package com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.Pet;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class PetMenu implements InventoryMenu {

    private ArrayList<Pet> petsForSale;

    public PetMenu() {
        petsForSale = Pet.getPets();
    }

    private Pet getPetFromIS(ItemStack i) {
        for (Pet p : petsForSale) {
            if (i.getType() == p.toItemStack().getType()) {
                return p;
            }
        }
        return null;
    }

    @Override
    public void onClick(Player p, ItemStack clicked, int clickedSlot) {
        if (p == null || clicked == null) {
            return;
        }

        if (clicked.getType() == Material.WOOL) {
            DyeColor color = DyeColor.getByWoolData(clicked.getData().getData());
            if (color == DyeColor.RED) {
                DatabaseManager.setActivePetForPlayer(User.getUser(p.getName()).getId(), -1);
                User u = User.getUser(p.getName());
                if (u.currentPet != null) {
                    u.currentPet.remove();
                }
                p.closeInventory();
                onClose(p);
                return;
            } else {
                ItemStack possiblePet = p.getOpenInventory().getItem(clickedSlot - 9);
                Pet thePet = getPetFromIS(possiblePet);
                if (thePet != null) {
                    DatabaseManager.setActivePetForPlayer(User.getUser(p.getName()).getId(), thePet.id);
                    User u = User.getUser(p.getName());
                    if (u.currentPet != null) {
                        u.currentPet.remove();
                    }
                    u.currentPet = DatabaseManager.getActivePetForPlayer(p).spawnPet();
                    p.closeInventory();
                    onClose(p);
                    return;
                } else {
                    return;
                }
            }
        }
        Pet pet = getPetFromIS(clicked);
        if (pet == null) {
            p.sendMessage(ChatColor.RED + "That is not a pet for sale.");
            p.closeInventory();
            onClose(p);
            return;
        }
        User u = User.getUser(p.getName());
        if (u.getTickets() < pet.price) {
            p.sendMessage(ChatColor.RED + "You do not have enough tickets to buy this pet.");
            p.closeInventory();
            onClose(p);
            return;
        }
        if (doesPlayerOwnPet(pet, DatabaseManager.getPetsOwnedForPlayer(p))) {
            return;
        }
        p.sendMessage(ChatColor.GREEN + "You have bought that pet!");
        p.sendMessage(ChatColor.GREEN + "You can set it's name by typing /setpetname <Name> while it is active!");
        DatabaseManager.addPetForPlayer(User.getUser(p.getName()).getId(), pet.id);
        DatabaseManager.setActivePetForPlayer(User.getUser(p.getName()).getId(), pet.id);
        DatabaseManager.removeTickets(u.getName(), pet.price);
        u.setTickets(u.getTickets() - pet.price);
        if (u.currentPet != null) {
            u.currentPet.remove();
        }
        Pet activePet = DatabaseManager.getActivePetForPlayer(p);
        if (activePet != null) {
            u.currentPet = DatabaseManager.getActivePetForPlayer(p).spawnPet();
        }
        p.closeInventory();
        onClose(p);
    }

    @Override
    public void onClose(Player p) {
        User user = User.getUser(p.getName());
        user.setMenuState(MenuState.NONE);
        p.getInventory().remove(p.getInventory().getItem(17));
    }

    public boolean doesPlayerOwnPet(Pet p, ArrayList<Pet> ownedPets) {
        for (Pet pet : ownedPets) {
            if (p.id == pet.id) {
                return true;
            }
        }
        return false;
    }

    public boolean isActivePet(Pet pet, Player p) {
        Pet active = DatabaseManager.getActivePetForPlayer(p);
        if (active == null) {
            return false;
        }
        if (active.id == pet.id) {
            return true;
        }

        return false;
    }

    @Override
    public void onOpen(Player p) {
        final User u = User.getUser(p.getName());
        Inventory petMenu = Bukkit.createInventory(null, 9 * 3, "Pets");
        ArrayList<Pet> playerPets = DatabaseManager.getPetsOwnedForPlayer(p);
        int petCounter = 0;
        for (Pet pet : petsForSale) {
            ItemStack petStack = pet.toItemStack();
            if (doesPlayerOwnPet(pet, playerPets)) {
                ItemMeta petMeta = petStack.getItemMeta();
                ArrayList<String> petLore = new ArrayList<String>();
                petLore.add(petMeta.getLore().get(0));
                petLore.add("You already own this pet.");
                petMeta.setLore(petLore);
                petStack.setItemMeta(petMeta);
                if (isActivePet(pet, p)) {
                    ItemStack redWool = new ItemStack(Material.WOOL, 1, DyeColor.RED.getData());
                    ItemMeta redWoolMeta = redWool.getItemMeta();
                    redWoolMeta.setDisplayName("Active Pet");
                    ArrayList<String> redWoolLore = new ArrayList<String>();
                    redWoolLore.add("This is your active pet.");
                    redWoolLore.add("Click here to deactivate your pet.");
                    redWoolMeta.setLore(redWoolLore);
                    redWool.setItemMeta(redWoolMeta);
                    petMenu.setItem(petCounter + 9, redWool);
                } else {
                    ItemStack blackWool = new ItemStack(Material.WOOL, 1, DyeColor.BLACK.getData());
                    ItemMeta blackWoolMeta = blackWool.getItemMeta();
                    blackWoolMeta.setDisplayName("Inactive Pet");
                    ArrayList<String> blackWoolLore = new ArrayList<String>();
                    blackWoolLore.add("You own this pet, but it is not your active pet.");
                    blackWoolLore.add("Click here to set it as your active pet.");
                    blackWoolMeta.setLore(blackWoolLore);
                    blackWool.setItemMeta(blackWoolMeta);
                    petMenu.setItem(petCounter + 9, blackWool);
                }
                petMenu.setItem(petCounter, petStack);
                petCounter++;
            } else {
                petMenu.setItem(petCounter, petStack);
                petCounter++;
            }
        }
        ItemStack ticketCount = new ItemStack(Material.PAPER);
        final ItemMeta ticketCountMeta = ticketCount.getItemMeta();
        ticketCountMeta.setDisplayName("Your Tickets");
        ArrayList<String> ticketCountString = new ArrayList<String>() {{
            add(u.getTickets() + " Tickets");
        }};
        ticketCountMeta.setLore(ticketCountString);
        ticketCount.setItemMeta(ticketCountMeta);
        p.getInventory().setItem(17, ticketCount);
        p.openInventory(petMenu); // Haha, whoops.
        u.setMenuState(getMenuState()); // Double whoops D:
    }

    @Override
    public MenuState getMenuState() {
        return MenuState.PET_MENU;
    }
}
