package com.mcprohosting.plugins.ssundee_hub.utilities.menu;

import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import org.reflections.Reflections;

public class MenuTools {

    private MenuTools() {
    }

    public static InventoryMenu getNewMenuForState(MenuState state) {
        if (state == MenuState.NONE) {
            return null;
        }
        Reflections r = new Reflections("com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus");
        for (Class<? extends InventoryMenu> c : r.getSubTypesOf(InventoryMenu.class)) {
            try {
                InventoryMenu instance = c.newInstance();
                MenuState classState = instance.getMenuState();
                if (classState == state) {
                    return instance;
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }
}
