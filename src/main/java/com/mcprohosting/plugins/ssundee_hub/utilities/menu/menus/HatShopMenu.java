package com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuTools;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.ProcessMenu;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class HatShopMenu implements InventoryMenu {

    private HatItem getHatFromMeta(ItemStack stack) {
        ItemMeta m = stack.getItemMeta();
        for (HatItem h : HatItem.getServerHats()) {
            if (m instanceof SkullMeta) {
                if (h.playerName.equalsIgnoreCase(((SkullMeta) m).getOwner())) {
                    return h;
                }
            } else {
                if (h.item == stack.getTypeId() && h.data == stack.getData().getData()) {
                    return h;
                }
            }
        }

        return null;
    }

    private boolean doesPlayerOwnHat(HatItem h, Player p) {
        ArrayList<HatItem> ownedHats = new ArrayList<HatItem>();
        ownedHats = DatabaseManager.getHatsForUser(User.getUser(p.getName()).getId());
        if (ownedHats.size() < 1) {
            return false;
        }

        for (HatItem hat : ownedHats) {
            if (hat.id == h.id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(Player p, ItemStack clicked, int clickedSlot) {
        if (p == null || clicked == null) {
            return;
        }

        User u = User.getUser(p.getName());
        HatItem hatToBuy = getHatFromMeta(clicked);
        if (hatToBuy == null) {
            p.sendMessage(ChatColor.RED + "That item can not be bought.");
            //p.closeInventory();
            //onClose(p);
        } else {
            if (u.getTickets() < hatToBuy.price) {
                p.sendMessage(ChatColor.RED + "You do not have enough tickets to buy this hat.");
                p.closeInventory();
                onClose(p);
                return;
            }
            if (doesPlayerOwnHat(hatToBuy, p)) {
                if (p.getInventory().getHelmet() != null) {
                    if (hatToBuy.displayName.equalsIgnoreCase(p.getInventory().getHelmet().getItemMeta().getDisplayName())
                            && (byte) hatToBuy.data == p.getInventory().getHelmet().getData().getData()) {
                        p.sendMessage(ChatColor.YELLOW + "Unequipping your hat!");
                        p.getInventory().setHelmet(null);
                    } else {
                        p.sendMessage(ChatColor.GREEN + "Equipping a different hat!");
                        ItemStack itemStack = clicked.clone();
                        itemStack.getItemMeta().setLore(null);
                        p.getInventory().setHelmet(itemStack);
                    }
                } else {
                    p.sendMessage(ChatColor.GREEN + "Equipping your hat!");
                    ItemStack itemStack = clicked.clone();
                    itemStack.getItemMeta().setLore(null);
                    p.getInventory().setHelmet(itemStack);
                }

                p.closeInventory();
                onClose(p);
                return;
            }
            u.setTickets(u.getTickets() - hatToBuy.price);
            DatabaseManager.removeTickets(p.getName(), hatToBuy.price);
            DatabaseManager.addHatForUser(User.getUser(p.getName()).getId(), hatToBuy.id);
            p.sendMessage(ChatColor.GREEN + "You bought the hat of " + hatToBuy.displayName + "!");
            p.closeInventory();
            onClose(p);
            return;
        }
    }

    @Override
    public void onClose(Player p) {
        User u = User.getUser(p.getName());
        u.setMenuState(MenuState.NONE);
        p.getInventory().remove(p.getInventory().getItem(17));
    }

    @Override
    public void onOpen(Player p) {
        final User u = User.getUser(p.getName());
        ArrayList<HatItem> hats = HatItem.getServerHats();

        // Populate inventory
        Inventory hatInventory = Bukkit.getServer().createInventory(null, 9 * 3, "Hat Shop"); //TODO Autofill Size
        for (HatItem h : hats) {
            hatInventory.addItem(h.toItemStack());
        }

        // Add ticket counter
        ItemStack ticketCount = new ItemStack(Material.PAPER);
        final ItemMeta ticketCountMeta = ticketCount.getItemMeta();
        ticketCountMeta.setDisplayName("Your Tickets");
        ArrayList<String> ticketCountString = new ArrayList<String>() {{
            add(u.getTickets() + " Tickets");
        }};
        ticketCountMeta.setLore(ticketCountString);
        ticketCount.setItemMeta(ticketCountMeta);
        p.getInventory().setItem(17, ticketCount);

        //Set state
        p.openInventory(hatInventory);
        u.setMenuState(getMenuState());
    }

    @Override
    public MenuState getMenuState() {
        return MenuState.HEAD_SHOP;
    }
}
