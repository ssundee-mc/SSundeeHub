package com.mcprohosting.plugins.ssundee_hub.utilities.map;

import com.mcprohosting.plugins.ssundee_hub.config.ConfigObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class ServerSign extends ConfigObject {

    public ServerSign() {
    }

    public ServerSign(Location location) {
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
    }

    public int x = 0;
    public int y = 0;
    public int z = 0;

    public Location getLocation() {
        return new Location(Bukkit.getWorlds().get(0), x, y, z);
    }

}
