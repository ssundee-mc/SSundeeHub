package com.mcprohosting.plugins.ssundee_hub.utilities.menu;

public enum MenuState {
    NONE,
    FRIEND_LIST,
    HEAD_SHOP,
    PET_MENU,
    VANISH_MENU,
    ACHIEVEMENT_MENU,
    SG_SHOP,
    TRAVEL_POWERS,
    TRAVEL_POWERS_SPEED,
    TRAVEL_POWERS_JUMP,
    TRAVEL_POWERS_TELEPORT,
    TRAVEL_POWERS_FLY;
}
