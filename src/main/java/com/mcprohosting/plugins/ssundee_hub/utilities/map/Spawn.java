package com.mcprohosting.plugins.ssundee_hub.utilities.map;

import com.mcprohosting.plugins.ssundee_hub.config.ConfigObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Spawn extends ConfigObject {

    public Spawn() {
    }

    public Spawn(Location location) {
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
        this.pitch = location.getPitch();
        this.yaw = location.getYaw();
    }

    public int x = 0;
    public int y = 0;
    public int z = 0;
    public double pitch = 0.0;
    public double yaw = 0.0;

    public Location getLocation() {
        return new Location(Bukkit.getWorlds().get(0), x, y, z, (float) yaw, (float) pitch);
    }

}
