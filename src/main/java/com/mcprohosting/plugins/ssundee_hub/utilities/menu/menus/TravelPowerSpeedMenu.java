package com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus;

import com.mcprohosting.plugins.ssundee_hub.database.Database;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class TravelPowerSpeedMenu implements InventoryMenu {
    @Override
    public void onClick(Player player, ItemStack clickedItem, int clickedSlot) {
        User user = User.getUser(player.getName());
        int ownedLevel = user.getOwnedTravelPowerLevel(TravelPower.SPEED);
        int playerLevel = user.getTravelPowerLevel(TravelPower.SPEED);
        int levelClicked = clickedSlot + 1;
        int cost = TravelPower.SPEED.getCostForLevel(levelClicked);

        if (levelClicked == ownedLevel + 1) {
            if (user.getTickets() >= cost) {
                user.setTickets(user.getTickets() - cost);
                user.setTravelPowerLevel(TravelPower.SPEED, levelClicked, false);
                DatabaseManager.removeTickets(user.getName(), cost);
                user.updateTravelPower(TravelPower.SPEED, false);
                user.refreshOwnedTravelPowers();
                onOpen(player);
            } else {
                player.sendMessage(ChatColor.RED + "You do not have enough tickets to purchase Speed " + levelClicked);
            }

            return;
        }

        if (levelClicked <= ownedLevel && levelClicked != playerLevel && levelClicked > 0) {
            player.sendMessage(ChatColor.GREEN + "Setting your speed level to " + levelClicked);
            user.setTravelPowerLevel(TravelPower.SPEED, levelClicked, true);
            user.updateTravelPower(TravelPower.SPEED, false);
        }
    }

    @Override
    public void onClose(Player player) {
        User user = User.getUser(player.getName());
        user.setMenuState(MenuState.NONE);
    }

    @Override
    public void onOpen(Player player) {
        User user = User.getUser(player.getName());
        Inventory inv = Bukkit.createInventory(null, 9, "Speed");
        int speed = user.getOwnedTravelPowerLevel(TravelPower.SPEED);

        // Populate inventory
        for (int i = 1; i <= 9; i++) {
            ItemStack item = new ItemStack(Material.SUGAR);
            ItemMeta meta = item.getItemMeta();
            ArrayList<String> lore = new ArrayList<>();

            if (i > speed + 1) {
                meta.setDisplayName(ChatColor.RED + "Speed " + i);
                lore.add(ChatColor.RESET + "Lower levels must be purchased first.");
            } else if (i == speed + 1) {
                meta.setDisplayName(ChatColor.BLUE + "Speed " + i);
                lore.add("Price: " + TravelPower.SPEED.getCostForLevel(speed + 1) + " Tickets");
            } else {
                meta.setDisplayName(ChatColor.GREEN + "Speed " + i);
            }

            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.setItem(i - 1, item);
        }

        player.openInventory(inv);
        user.setMenuState(getMenuState());
    }

    @Override
    public MenuState getMenuState() {
        return MenuState.TRAVEL_POWERS_SPEED;
    }
}
