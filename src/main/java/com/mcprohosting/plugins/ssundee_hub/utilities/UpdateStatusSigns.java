package com.mcprohosting.plugins.ssundee_hub.utilities;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerSigns;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerStatus;

public class UpdateStatusSigns implements Runnable {

    @Override
    public void run() {
        for (String server : ServerSigns.getServers()) {
            String prefix = server.substring(0, 2);
            String id = server.substring(2, server.length());
            ServerStatus status = DatabaseManager.getServerStatus(prefix, Integer.parseInt(id));
            if (status != null) {
                ServerSigns.updateStatus(server, status.getCurrentPlayers(), status.getMaxPlayers(), status.getStatus());
            }
        }
    }

}
