package com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuTools;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

import java.util.ArrayList;

public class TravelPowerMenu implements InventoryMenu {
    private static final int TOGGLE_SPEED_SLOT = 10;
    private static final int TOGGLE_JUMP_SLOT = 12;
    private static final int TOGGLE_FLY_SLOT = 16;

    private static ItemStack disabledItem;
    private static ItemStack enabledItem;

    static {
        disabledItem = new Wool(DyeColor.BLACK).toItemStack();
        ItemMeta disabledMeta = disabledItem.getItemMeta();
        disabledMeta.setDisplayName(ChatColor.RESET + "Click to enable travel power!");
        disabledItem.setItemMeta(disabledMeta);

        enabledItem = new Wool(DyeColor.RED).toItemStack();
        ItemMeta enabledMeta = enabledItem.getItemMeta();
        enabledMeta.setDisplayName(ChatColor.RESET + "Click to disable travel power!");
        enabledItem.setItemMeta(enabledMeta);
    }

    @Override
    public void onClick(Player player, ItemStack clickedItem, int clickedSlot) {
        if (clickedItem == null) {
            return;
        }

        if (clickedItem.getType().equals(Material.SUGAR)) {
            MenuTools.getNewMenuForState(MenuState.TRAVEL_POWERS_SPEED).onOpen(player);
            return;
        }

        if (clickedItem.getType().equals(Material.GOLD_BOOTS)) {
            MenuTools.getNewMenuForState(MenuState.TRAVEL_POWERS_JUMP).onOpen(player);
            return;
        }

        if (clickedItem.getType().equals(Material.ENDER_PEARL)) {
            MenuTools.getNewMenuForState(MenuState.TRAVEL_POWERS_TELEPORT).onOpen(player);
            return;
        }

        if (clickedItem.getType().equals(Material.FEATHER)) {
            if (isFlyAvailable(player.getName())) {
                MenuTools.getNewMenuForState(MenuState.TRAVEL_POWERS_FLY).onOpen(player);
            } else {
                player.sendMessage(ChatColor.RED + "All other powers must be maxed out before purchasing fly powers!");
                player.closeInventory();
                onClose(player);
            }

            return;
        }

        if (clickedItem.getType().equals(Material.WOOL)) {
            player.setItemOnCursor(null);
            togglePower(player, clickedItem, clickedSlot);
        }
    }

    @Override
    public void onClose(Player player) {
        User user = User.getUser(player.getName());
        user.setMenuState(MenuState.NONE);
    }

    @Override
    public void onOpen(Player player) {
        User user = User.getUser(player.getName());
        Inventory inv = Bukkit.createInventory(null, 9 * 2, "Travel Powers!");
        int speed = user.getTravelPowerLevel(TravelPower.SPEED);
        int jump = user.getTravelPowerLevel(TravelPower.JUMP);
        int tp = user.getTravelPowerLevel(TravelPower.TELEPORT);
        int fly = user.getTravelPowerLevel(TravelPower.FLY);

        // Populate inventory
        ItemStack speedItem = new ItemStack(Material.SUGAR);
        ItemMeta speedItemMeta = speedItem.getItemMeta();
        speedItemMeta.setDisplayName(ChatColor.BOLD + "Speed Boost");
        ArrayList<String> speedLore = new ArrayList<>();
        speedLore.add("Level: " + speed);
        speedItemMeta.setLore(speedLore);
        speedItem.setItemMeta(speedItemMeta);

        ItemStack jumpItem = new ItemStack(Material.GOLD_BOOTS);
        ItemMeta jumpItemMeta = jumpItem.getItemMeta();
        jumpItemMeta.setDisplayName(ChatColor.BOLD + "Jump Boost");
        ArrayList<String> jumpLore = new ArrayList<>();
        jumpLore.add("Level: " + jump);
        if (user.getTravelPowerLevel(TravelPower.DOUBLE_JUMP) == 1) {
            jumpLore.add("+Double Jump");
        }
        jumpItemMeta.setLore(jumpLore);
        jumpItem.setItemMeta(jumpItemMeta);

        ItemStack teleportItem = new ItemStack(Material.ENDER_PEARL);
        ItemMeta teleportItemMeta = teleportItem.getItemMeta();
        teleportItemMeta.setDisplayName(ChatColor.BOLD + "Teleport");
        ArrayList<String> teleportLore = new ArrayList<>();
        teleportLore.add("Level: " + tp);
        if (user.getTravelPowerLevel(TravelPower.TP_LOCATION) == 1) {
            teleportLore.add("+Teleport to Location");
        }
        if (user.getTravelPowerLevel(TravelPower.TP_PlAYER) == 1) {
            teleportLore.add("+Teleport to Player");
        }
        teleportItemMeta.setLore(teleportLore);
        teleportItem.setItemMeta(teleportItemMeta);

        ItemStack flyItem = new ItemStack(Material.FEATHER);
        ItemMeta flyItemMeta = flyItem.getItemMeta();
        flyItemMeta.setDisplayName(ChatColor.BOLD + "Flying");
        ArrayList<String> flyLore = new ArrayList<>();
        if (isFlyAvailable(player.getName())) {
            flyLore.add("Level: " + fly);
        } else {
            flyLore.add(ChatColor.RED + "All other powers must be maxed");
            flyLore.add(ChatColor.RED + "out before purchasing fly powers!");
        }
        flyItemMeta.setLore(flyLore);
        flyItem.setItemMeta(flyItemMeta);

        inv.setItem(1, speedItem);
        inv.setItem(3, jumpItem);
        inv.setItem(5, teleportItem);
        inv.setItem(7, flyItem);
        inv.setItem(TOGGLE_SPEED_SLOT, user.isTravelPowerActive(TravelPower.SPEED) ? enabledItem : disabledItem);
        inv.setItem(TOGGLE_JUMP_SLOT, user.isTravelPowerActive(TravelPower.JUMP) ? enabledItem : disabledItem);
        inv.setItem(TOGGLE_FLY_SLOT, user.isTravelPowerActive(TravelPower.FLY) ? enabledItem : disabledItem);

        player.openInventory(inv);
        user.setMenuState(getMenuState());
    }

    @Override
    public MenuState getMenuState() {
        return MenuState.TRAVEL_POWERS;
    }

    private boolean isFlyAvailable(String player) {
        User user = User.getUser(player);
        int speed = user.getOwnedTravelPowerLevel(TravelPower.SPEED);
        int jump = user.getOwnedTravelPowerLevel(TravelPower.JUMP);
        int tp = user.getOwnedTravelPowerLevel(TravelPower.TELEPORT);

        return (speed == TravelPower.SPEED.getMaxLevel()) && (jump == TravelPower.JUMP.getMaxLevel()) && (tp == TravelPower.TELEPORT.getMaxLevel());
    }

    private void togglePower(Player player, ItemStack clicked, int clickedSlot) {
        User user = User.getUser(player.getName());
        TravelPower power = getPowerFromToggleSlot(clickedSlot);

        if (power == null) {
            return;
        }

        int powerLevel = user.getTravelPowerLevel(power);

        if (powerLevel > 0) {
            user.updateTravelPower(power, true);
        } else {
            player.sendMessage(ChatColor.RED + "You must first purchase this travel power before activating it.");
        }

        onOpen(player);
    }

    private TravelPower getPowerFromToggleSlot(int slot) {
        switch (slot) {
            case TOGGLE_SPEED_SLOT:
                return TravelPower.SPEED;
            case TOGGLE_JUMP_SLOT:
                return TravelPower.JUMP;
            case TOGGLE_FLY_SLOT:
                return TravelPower.FLY;
            default:
                return null;
        }
    }
}
