package com.mcprohosting.plugins.ssundee_hub.utilities;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LocationUtil {

    private static Map<String, ArrayList<Location>> editors;

    static {
        editors = new HashMap<String, ArrayList<Location>>();
    }

    public static boolean isEditing(String name) {
        return editors.containsKey(name);
    }

    public static void startEditing(String name) {
        editors.put(name, new ArrayList<Location>());
    }

    public static void addLocation(String name, Location location) {
        if (editors.containsKey(name) == false) {
            editors.put(name, new ArrayList<Location>());
        }

        editors.get(name).add(location);
    }

    public static void clearLocations(String name) {
        if (editors.containsKey(name) == false) {
            editors.put(name, new ArrayList<Location>());
        }

        editors.get(name).clear();
    }

    public static ArrayList<Location> getLocations(String name) {
        if (editors.containsKey(name) == false) {
            editors.put(name, new ArrayList<Location>());
        }

        return editors.get(name);
    }

    public static ArrayList<Location> finishEditing(String name) {
        if (editors.containsKey(name) == false) {
            return new ArrayList<Location>();
        }

        return editors.remove(name);
    }

}
