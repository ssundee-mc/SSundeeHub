package com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class TravelPowerJumpMenu implements InventoryMenu {
    private final static int DOUBLE_JUMP_SLOT = 13;

    @Override
    public void onClick(Player player, ItemStack clickedItem, int clickedSlot) {
        User user = User.getUser(player.getName());
        int ownedLevel = user.getOwnedTravelPowerLevel(TravelPower.JUMP);
        int playerLevel = user.getTravelPowerLevel(TravelPower.JUMP);
        int levelClicked = clickedSlot + 1;
        int cost = TravelPower.JUMP.getCostForLevel(levelClicked);

        if (clickedSlot == DOUBLE_JUMP_SLOT) {
            if (user.getOwnedTravelPowerLevel(TravelPower.DOUBLE_JUMP) == 0 && ownedLevel >= 5) {
                if (user.getTickets() >= TravelPower.DOUBLE_JUMP.getCostForLevel(1)) {
                    user.setTickets(user.getTickets() - TravelPower.DOUBLE_JUMP.getCostForLevel(1));
                    user.setTravelPowerLevel(TravelPower.DOUBLE_JUMP, 1, false);
                    user.refreshOwnedTravelPowers();
                    onOpen(player);
                } else {
                    player.sendMessage(ChatColor.RED + "You do not have enough tickets to purchase Double Jump.");
                }
            }

            return;
        }

        if (levelClicked == ownedLevel + 1) {
            if (user.getTickets() >= cost) {
                user.setTickets(user.getTickets() - cost);
                DatabaseManager.removeTickets(user.getName(), cost);
                user.setTravelPowerLevel(TravelPower.JUMP, levelClicked, false);
                user.updateTravelPower(TravelPower.JUMP, false);
                user.refreshOwnedTravelPowers();
                onOpen(player);
            } else {
                player.sendMessage(ChatColor.RED + "You do not have enough tickets to purchase Jump " + levelClicked);
            }

            return;
        }

        if (levelClicked <= ownedLevel && levelClicked != playerLevel && levelClicked > 0) {
            player.sendMessage(ChatColor.GREEN + "Setting your jump level to " + levelClicked);
            user.setTravelPowerLevel(TravelPower.JUMP, levelClicked, true);
            user.updateTravelPower(TravelPower.JUMP, false);
        }
    }

    @Override
    public void onClose(Player player) {
        User user = User.getUser(player.getName());
        user.setMenuState(MenuState.NONE);
    }

    @Override
    public void onOpen(Player player) {
        User user = User.getUser(player.getName());
        Inventory inv = Bukkit.createInventory(null, 9 * 2, "Jump");
        int jump = user.getOwnedTravelPowerLevel(TravelPower.JUMP);

        // Populate inventory
        for (int i = 1; i <= 9; i++) {
            ItemStack item = new ItemStack(Material.GOLD_BOOTS);
            ItemMeta meta = item.getItemMeta();
            ArrayList<String> lore = new ArrayList<>();

            if (i > jump + 1) {
                meta.setDisplayName(ChatColor.RED + "Jump " + i);
                lore.add(ChatColor.RESET + "Lower levels must be purchased first.");
            } else if (i == jump + 1) {
                meta.setDisplayName(ChatColor.BLUE + "Jump " + i);
                lore.add("Price: " + TravelPower.JUMP.getCostForLevel(jump + 1) + " Tickets");
            } else {
                meta.setDisplayName(ChatColor.GREEN + "Jump " + i);
            }

            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.setItem(i - 1, item);
        }

        boolean doubleJumpPurchased = (user.getTravelPowerLevel(TravelPower.DOUBLE_JUMP) == 1);
        ItemStack doubleJumpItem = new ItemStack(Material.DIAMOND_BOOTS);
        ItemMeta doubleJumpMeta = doubleJumpItem.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();

        if (doubleJumpPurchased) {
            doubleJumpMeta.setDisplayName(ChatColor.GREEN + "Double Jump");
        } else if (jump >= 5) {
            doubleJumpMeta.setDisplayName(ChatColor.BLUE + "Double Jump");
            lore.add("Price: " + TravelPower.DOUBLE_JUMP.getCostForLevel(1) + " Tickets");
        } else {
            doubleJumpMeta.setDisplayName(ChatColor.RED + "Double Jump");
            lore.add(ChatColor.RESET + "Jump level 5 must be purchased first.");
        }

        doubleJumpMeta.setLore(lore);
        doubleJumpItem.setItemMeta(doubleJumpMeta);
        //inv.setItem(DOUBLE_JUMP_SLOT, doubleJumpItem);

        player.openInventory(inv);
        user.setMenuState(getMenuState());
    }

    @Override
    public MenuState getMenuState() {
        return MenuState.TRAVEL_POWERS_JUMP;
    }
}
