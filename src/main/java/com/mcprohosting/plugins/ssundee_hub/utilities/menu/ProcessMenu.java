package com.mcprohosting.plugins.ssundee_hub.utilities.menu;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.items.HatItem;
import com.mcprohosting.plugins.ssundee_hub.utilities.UtilityMethods;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ProcessMenu implements Runnable {

    private Player player;

    public ProcessMenu(Player p) {
        this.player = p;
    }

    public static void fillPlayerInventoryWithItems(Player p) {
        User user = User.getUser(p.getName());
        ArrayList<String> comingSoonText = new ArrayList<String>();
        comingSoonText.add("Coming Soon!");

        // Friend List
        ItemStack friendListBook = new ItemStack(Material.BOOK);
        ItemMeta friendListMeta = friendListBook.getItemMeta();
        friendListMeta.setDisplayName("Friend List");
        friendListMeta.setLore(comingSoonText);
        friendListBook.setItemMeta(friendListMeta);
        p.getInventory().setItem(0, friendListBook);

        // Hat Shop
        ItemStack headShop = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        ItemMeta headShopMeta = headShop.getItemMeta();
        headShopMeta.setDisplayName("Hat Shop");
        headShop.setItemMeta(headShopMeta);
        p.getInventory().setItem(1, headShop);


        // Pet Shop
        ItemStack petItem = new ItemStack(Material.BONE);
        ItemMeta petItemMeta = petItem.getItemMeta();
        petItemMeta.setDisplayName("Pet Menu");
        petItemMeta.setLore(comingSoonText);
        petItem.setItemMeta(petItemMeta);
        p.getInventory().setItem(2, petItem);

        // Vanish
        ItemStack vanishItem = new ItemStack(Material.WATCH);
        ItemMeta vanishItemMeta = vanishItem.getItemMeta();
        vanishItemMeta.setDisplayName("Player Vanish Settings");
        vanishItem.setItemMeta(vanishItemMeta);
        p.getInventory().setItem(3, vanishItem);

        // Achievements
        ItemStack achievements = new ItemStack(Material.NETHER_STAR);
        ItemMeta achievementsMeta = achievements.getItemMeta();
        achievementsMeta.setDisplayName("Achievements List");
        achievementsMeta.setLore(comingSoonText);
        achievements.setItemMeta(achievementsMeta);
        p.getInventory().setItem(4, achievements);

        // Filler
        p.getInventory().setItem(5, new ItemStack(Material.AIR));
        p.getInventory().setItem(6, new ItemStack(Material.AIR));

        // Teleport
        if (user.getTravelPowerLevel(TravelPower.TELEPORT) > 0) {
            ItemStack teleport = new ItemStack(Material.ENDER_PEARL);
            ItemMeta teleportMeta = teleport.getItemMeta();
            teleportMeta.setDisplayName("Teleport");
            teleport.setItemMeta(teleportMeta);
            p.getInventory().setItem(7, teleport);
        } else {
            p.getInventory().setItem(7, new ItemStack(Material.AIR));
        }

        // Travel Powers
        ItemStack tPowersMenu = new ItemStack(Material.EMERALD);
        ItemMeta tPowersMenuMeta = tPowersMenu.getItemMeta();
        tPowersMenuMeta.setDisplayName("Travel Powers Menu");
        tPowersMenuMeta.setLore(comingSoonText);
        tPowersMenu.setItemMeta(tPowersMenuMeta);
        p.getInventory().setItem(8, tPowersMenu);
    }

    @Override
    public void run() {
        if (player == null) {
            return;
        }
        User user = User.getUser(player.getName());
        if (user.getCurrentMenuState() == MenuState.NONE) {
            ItemStack selectedItem = player.getItemInHand();
            switch (selectedItem.getType()) {
                default:
                    break;
                case BOOK:
                    // Friend List
                    player.sendMessage("Coming Soon!");
                    break;
                case SKULL_ITEM:
                    // Hat Shop
                    MenuTools.getNewMenuForState(MenuState.HEAD_SHOP).onOpen(player);
                    break;
                case BONE:
                    // Pet Menu
                    MenuTools.getNewMenuForState(MenuState.PET_MENU).onOpen(player);
                    break;
                case WATCH:
                    if ((System.currentTimeMillis() - user.getLastVanishClick()) > 5000) {
                        if (user.getVanishState() == 0) {
                            player.sendMessage(ChatColor.GREEN + "Now vanishing all other players!");
                            user.setVanishState();
                            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                                player.hidePlayer(p);
                            }
                        } else if (user.getVanishState() == 1) {
                            //
                            player.sendMessage(ChatColor.GREEN + "Now showing Staff and Special Guests!");
                            user.setVanishState();
                            for (User person : User.getUsers()) {
                                Player p = Bukkit.getPlayer(person.getName());

                                if (p == null || p == player) {
                                    continue;
                                }

                                switch (person.getRank().toString().toLowerCase()) {
                                    case "youtuber":
                                        player.showPlayer(p);
                                    case "mod":
                                        player.showPlayer(p);
                                    case "admin":
                                        player.showPlayer(p);
                                    case "dev":
                                        player.showPlayer(p);
                                    case "owner":
                                        player.showPlayer(p);
                                }
                            }
                        } else {
                            player.sendMessage(ChatColor.GREEN + "Now showing all other players!");
                            user.setVanishState();
                            for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                                player.showPlayer(p);
                            }
                        }

                        user.setLastVanishClick(System.currentTimeMillis());
                    } else {
                        long seconds = ((5000 - (System.currentTimeMillis() - user.getLastVanishClick())) / 1000);
                        player.sendMessage(ChatColor.RED + "You must wait " + seconds + " seconds before you can toggle vanish again.");
                    }
                    //player.sendMessage("Coming Soon!");
                    break;
                case NETHER_STAR:
                    // Achievements
                    player.sendMessage("Coming Soon!");
                    break;
                case ENDER_PEARL:
                    // Teleport
                    if (System.currentTimeMillis() - user.getLastTeleportClick() > 2000) {
                        Location location = UtilityMethods.calculateTeleportLocation(player);
                        if (location != null) {
                            player.teleport(UtilityMethods.calculateTeleportLocation(player), TeleportCause.PLUGIN);
                            user.setLastTeleportClick(System.currentTimeMillis());
                        } else {
                            player.sendMessage(ChatColor.RED + "Teleport location out of range.");
                        }
                    } else {
                        long seconds = (2000 - (System.currentTimeMillis() - user.getLastTeleportClick())) / 1000;
                        player.sendMessage(ChatColor.RED + "You must wait " + seconds + " seconds before you can teleport again.");
                    }
                    break;
                case EMERALD:
                    // Travel Powers
                    MenuTools.getNewMenuForState(MenuState.TRAVEL_POWERS).onOpen(player);
                    break;
            }
        }
    }


}
