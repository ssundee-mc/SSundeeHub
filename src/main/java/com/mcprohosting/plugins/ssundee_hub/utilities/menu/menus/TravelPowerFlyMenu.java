package com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.utilities.user.TravelPower;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class TravelPowerFlyMenu implements InventoryMenu {
    @Override
    public void onClick(Player player, ItemStack clickedItem, int clickedSlot) {
        User user = User.getUser(player.getName());
        int ownedLevel = user.getOwnedTravelPowerLevel(TravelPower.FLY);
        int playerLevel = user.getTravelPowerLevel(TravelPower.FLY);
        int levelClicked = clickedSlot + 1;
        int cost = TravelPower.FLY.getCostForLevel(levelClicked);

        if (levelClicked == ownedLevel + 1) {
            if (user.getTickets() >= cost) {
                user.setTickets(user.getTickets() - cost);
                DatabaseManager.removeTickets(user.getName(), cost);
                user.setTravelPowerLevel(TravelPower.FLY, levelClicked, false);
                user.updateTravelPower(TravelPower.FLY, false);
                user.refreshOwnedTravelPowers();
                onOpen(player);
            } else {
                player.sendMessage(ChatColor.RED + "You do not have enough tickets to purchase Fly " + levelClicked);
            }

            return;
        }

        if (levelClicked <= ownedLevel && levelClicked != playerLevel && levelClicked > 0) {
            player.sendMessage(ChatColor.GREEN + "Setting your fly level to " + levelClicked);
            user.setTravelPowerLevel(TravelPower.FLY, levelClicked, true);
            user.updateTravelPower(TravelPower.FLY, false);
        }
    }

    @Override
    public void onClose(Player player) {
        User user = User.getUser(player.getName());
        user.setMenuState(MenuState.NONE);
    }

    @Override
    public void onOpen(Player player) {
        User user = User.getUser(player.getName());
        Inventory inv = Bukkit.createInventory(null, 9, "Fly");
        int fly = user.getOwnedTravelPowerLevel(TravelPower.FLY);

        // Populate inventory
        for (int i = 1; i <= 9; i++) {
            ItemStack item = new ItemStack(Material.FEATHER);
            ItemMeta meta = item.getItemMeta();
            ArrayList<String> lore = new ArrayList<>();

            if (i > fly + 1) {
                meta.setDisplayName(ChatColor.RED + "Fly " + i);
                lore.add(ChatColor.RESET + "Lower levels must be purchased first.");
            } else if (i == fly + 1) {
                meta.setDisplayName(ChatColor.BLUE + "Fly " + i);
                lore.add("Price: " + TravelPower.FLY.getCostForLevel(fly + 1) + " Tickets");
            } else {
                meta.setDisplayName(ChatColor.GREEN + "Fly " + i);
            }

            meta.setLore(lore);
            item.setItemMeta(meta);
            inv.setItem(i - 1, item);
        }

        player.openInventory(inv);
        user.setMenuState(getMenuState());
    }

    @Override
    public MenuState getMenuState() {
        return MenuState.TRAVEL_POWERS_FLY;
    }
}
