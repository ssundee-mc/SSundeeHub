package com.mcprohosting.plugins.ssundee_hub.utilities.user;

public enum TravelPower {
    SPEED("SPEED", 50, 9),
    JUMP("JUMP", 50, 9),
    DOUBLE_JUMP("DOUBLE_JUMP", 400, 1),
    TELEPORT("TELEPORT", 50, 9),
    TP_LOCATION("TP_LOCATION", 50, 1),
    TP_PlAYER("TP_PLAYER", 100, 1),
    FLY("FLY", 200, 9);

    private final String power;
    private final int costPerLevel;
    private final int maxLevel;

    TravelPower(String power, int costPerLevel, int maxLevel) {
        this.power = power;
        this.costPerLevel = costPerLevel;
        this.maxLevel = maxLevel;
    }

    @Override
    public String toString() {
        return power;
    }

    public int getCostForLevel(int level) {
        switch (this) {
            case FLY:
                return 5 + (costPerLevel * level);
            default:
                return costPerLevel * level;
        }
    }

    public int getMaxLevel() {
        return maxLevel;
    }
}
