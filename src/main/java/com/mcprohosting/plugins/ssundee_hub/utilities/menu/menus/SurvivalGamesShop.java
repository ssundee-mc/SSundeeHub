package com.mcprohosting.plugins.ssundee_hub.utilities.menu.menus;

import com.mcprohosting.plugins.ssundee_hub.database.DatabaseManager;
import com.mcprohosting.plugins.ssundee_hub.entities.User;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.InventoryMenu;
import com.mcprohosting.plugins.ssundee_hub.utilities.menu.MenuState;
import com.mcprohosting.plugins.ssundee_hub.items.MinigameShopItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class SurvivalGamesShop implements InventoryMenu {

    public SurvivalGamesShop() {
        this.shopItems = DatabaseManager.getMinigameShopItemsFromDatabase("sg");
    }

    private List<MinigameShopItem> shopItems = new ArrayList<MinigameShopItem>();

    private MinigameShopItem getItemFromType(Material m) {
        for (MinigameShopItem sssi : shopItems) {
            if (sssi.type == m) {
                return sssi;
            }
        }
        return null;
    }

    @Override
    public void onClick(Player p, ItemStack clicked, int clickedSlot) {
        if (p == null || clicked == null) {
            return;
        }

        User user = User.getUser(p.getName());

        if (user == null) {
            return;
        }

        if (clicked.getType() == Material.PAPER) {
            return;
        } else {
            MinigameShopItem item = getItemFromType(clicked.getType());
            if (item == null) {
                return;
            }
            if (item.price > user.getTickets()) {
                p.sendMessage(ChatColor.RED + "You do not have enough tickets to buy this item!");
                p.closeInventory();
                onClose(p);
            } else {
                DatabaseManager.addItemForPlayer(item.id, User.getUser(p.getName()).getId());
                user.setTickets(user.getTickets() - item.price);
                DatabaseManager.removeTickets(user.getName(), item.price);
                p.sendMessage(ChatColor.GREEN + "You bought a " + item.name + "!");
                p.sendMessage(ChatColor.GREEN + "Use /minigameItems to see your purchased items!"); //TODO temp until purchase book is implemented
                p.closeInventory();
                onClose(p);
            }
        }
    }

    @Override
    public void onClose(Player p) {
        User u = User.getUser(p.getName());
        u.setMenuState(MenuState.NONE);
    }

    @Override
    public void onOpen(Player p) {
        final User user = User.getUser(p.getName());
        Inventory shopItems = Bukkit.getServer().createInventory(null, 9, "Survival Games Shop!");
        ItemStack ticketCount = new ItemStack(Material.PAPER);
        final ItemMeta ticketCountMeta = ticketCount.getItemMeta();
        ticketCountMeta.setDisplayName("Your Tickets");
        ArrayList<String> ticketCountString = new ArrayList<String>() {{
            add(user.getTickets() + " Tickets");
        }};
        ticketCountMeta.setLore(ticketCountString);
        ticketCount.setItemMeta(ticketCountMeta);
        shopItems.setItem(8, ticketCount);
        for (MinigameShopItem i : this.shopItems) {
            ItemStack newItem = new ItemStack(i.type);
            ItemMeta newItemMeta = newItem.getItemMeta();
            newItemMeta.setDisplayName(i.name);
            final int price = i.price;
            final String description = i.description;
            ArrayList<String> priceString = new ArrayList<String>() {{
                add(description);
                add(price + " Tickets");
            }};
            newItemMeta.setLore(priceString);
            newItem.setItemMeta(newItemMeta);
            shopItems.addItem(newItem);
        }

        p.openInventory(shopItems);
        user.setMenuState(getMenuState());

    }

    @Override
    public MenuState getMenuState() {
        return MenuState.SG_SHOP;
    }
}
