package com.mcprohosting.plugins.ssundee_hub.utilities.menu;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface InventoryMenu {
    public void onClick(Player p, ItemStack clicked, int clickedSlot);

    public void onClose(Player p);

    public void onOpen(Player p);

    public MenuState getMenuState();
}
