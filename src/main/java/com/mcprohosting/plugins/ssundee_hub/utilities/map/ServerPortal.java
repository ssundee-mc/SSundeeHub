package com.mcprohosting.plugins.ssundee_hub.utilities.map;

import com.mcprohosting.plugins.ssundee_hub.SSundeeHub;
import com.mcprohosting.plugins.ssundee_hub.config.ConfigObject;
import com.mcprohosting.plugins.ssundee_hub.servers.ServerPortals;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class ServerPortal extends ConfigObject {

    public ServerPortal() {
    }

    public ServerPortal(Location cornerOne, Location cornerTwo) {
        x1 = (cornerOne.getBlockX() < cornerTwo.getBlockX()) ? cornerOne.getBlockX() : cornerTwo.getBlockX();
        x2 = (cornerOne.getBlockX() > cornerTwo.getBlockX()) ? cornerOne.getBlockX() : cornerTwo.getBlockX();
        y1 = (cornerOne.getBlockY() < cornerTwo.getBlockY()) ? cornerOne.getBlockY() : cornerTwo.getBlockY();
        y2 = (cornerOne.getBlockY() > cornerTwo.getBlockY()) ? cornerOne.getBlockY() : cornerTwo.getBlockY();
        z1 = (cornerOne.getBlockZ() < cornerTwo.getBlockZ()) ? cornerOne.getBlockZ() : cornerTwo.getBlockZ();
        z2 = (cornerOne.getBlockZ() > cornerTwo.getBlockZ()) ? cornerOne.getBlockZ() : cornerTwo.getBlockZ();
    }

    public int x1 = 0;
    public int x2 = 0;
    public int y1 = 0;
    public int y2 = 0;
    public int z1 = 0;
    public int z2 = 0;

    public boolean entered(Player player) {
        for (int x = x1; x <= x2; x++) {
            for (int y = y1; y <= y2; y++) {
                for (int z = z1; z <= z2; z++) {
                    if (ServerPortals.standingInBlock(player, x, y, z)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean containsBlock(Block block) {
        SSundeeHub.getPlugin().getLogger().info(block.getLocation().toString());
        for (int x = x1; x <= x2; x++) {
            for (int y = y1; y <= y2; y++) {
                for (int z = z1; z <= z2; z++) {
                    World world = block.getWorld();
                    Block portalBlock = world.getBlockAt(x, y, z);
                    if (block.equals(portalBlock)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}
